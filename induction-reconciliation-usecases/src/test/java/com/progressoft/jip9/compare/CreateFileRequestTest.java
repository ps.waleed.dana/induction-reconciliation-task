package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.TypeData;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.request.CreateFileRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CreateFileRequestTest {

    @Test
    public void givenCreateFileRequest_whenSetListNull_thenFail(){
        List<Transaction> list= null;
        Path path = Paths.get("");
        FilesExtension filesExtension = FilesExtension.CSV;
        TypeData typeData = TypeData.MISSING;

        NullPointerException nullPointerException =
                Assertions.assertThrows(NullPointerException.class,
                        () -> new CreateFileRequest(list, path, filesExtension, typeData));

        Assertions.assertEquals("the list is null", nullPointerException.getMessage());
    }

    @Test
    public void givenCreateFileRequest_whenSetPathNull_thenFail(){
        List<Transaction> list= new ArrayList<>();
        Path path = null;
        FilesExtension filesExtension = FilesExtension.CSV;
        TypeData typeData = TypeData.MISSING;

        NullPointerException nullPointerException =
                Assertions.assertThrows(NullPointerException.class,
                        () -> new CreateFileRequest(list, path, filesExtension, typeData));

        Assertions.assertEquals("the path is null", nullPointerException.getMessage());
    }

    @Test
    public void givenCreateFileRequest_whenSetFileExtensionNull_thenFail(){
        List<Transaction> list= new ArrayList<>();
        Path path = Paths.get("");
        FilesExtension filesExtension = null;
        TypeData typeData = TypeData.MISSING;

        NullPointerException nullPointerException =
                Assertions.assertThrows(NullPointerException.class,
                        () -> new CreateFileRequest(list, path, filesExtension, typeData));

        Assertions.assertEquals("the file extension is null", nullPointerException.getMessage());
    }

    @Test
    public void givenCreateFileRequest_whenSetTypeDataNull_thenFail(){
        List<Transaction> list= new ArrayList<>();
        Path path = Paths.get("");
        FilesExtension filesExtension = FilesExtension.CSV;
        TypeData typeData = null;

        NullPointerException nullPointerException =
                Assertions.assertThrows(NullPointerException.class,
                        () -> new CreateFileRequest(list, path, filesExtension, typeData));

        Assertions.assertEquals("the type data is null", nullPointerException.getMessage());
    }



}
