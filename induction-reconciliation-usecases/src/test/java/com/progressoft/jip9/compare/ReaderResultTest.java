package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.result.ReaderResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

public class ReaderResultTest {

    @Test
    public void givenReaderResult_whenConstructNullListParameterList_thenFail(){
        List<Transaction> list = null;
        NullPointerException the_source_list_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> new ReaderResult(list), "the list is null");
        Assertions.assertEquals("the list is null", the_source_list_is_null.getMessage());
    }

    @Test
    public void givenReaderResult_whenWhenConstructParametersListNotNull_thenCanGetTheseLists(){
        List<Transaction> list = new LinkedList<>();
        ReaderResult result = new ReaderResult(list);
        Assertions.assertEquals(list, result.getList());
    }
}
