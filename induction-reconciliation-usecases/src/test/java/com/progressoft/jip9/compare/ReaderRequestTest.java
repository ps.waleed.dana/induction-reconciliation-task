package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.request.ReaderRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReaderRequestTest {

    @Test
    public void givenNullPathToFile_whenRead_thenFail() {
        Path path = null;
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new ReaderRequest(path, FilesExtension.CSV, FilesType.SOURCE));
        Assertions.assertEquals("null path", thrown.getMessage());
    }

    @Test
    public void givenNotExistsPath_whenRead_thenFail() {
        Path path = Paths.get("..", "sample-files" + "input-files", "bank.csv");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ReaderRequest(path, FilesExtension.CSV, FilesType.SOURCE));
        Assertions.assertEquals("path does not exists", thrown.getMessage());
    }

    @Test
    public void givenValidDirectoryPath_whenRead_thenFail() throws IOException {
        Path path = Files.createTempDirectory("bank-transactions");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ReaderRequest(path, FilesExtension.CSV, FilesType.SOURCE));
        Assertions.assertEquals("path is not a file", thrown.getMessage());
    }

    @Test
    public void givenValidFilesPathSourceAndTargetAndNullType_whenRead_thenFail(){
        Path path = Paths.get("..", "sample-files", "input-files", "bank-transactions.csv");
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new ReaderRequest(path, null, FilesType.SOURCE));
        Assertions.assertEquals("null extension file", thrown.getMessage());
    }

    @Test
    public void givenValidFilesPathSourceAndTargetAndValidType_whenGetPaths_thenReturnAsExpected(){
        Path path = Paths.get("..", "sample-files", "input-files", "bank-transactions.csv");
        ReaderRequest request = new ReaderRequest(path, FilesExtension.JSON, FilesType.SOURCE);
        Assertions.assertEquals(path, request.getPath());
    }
}
