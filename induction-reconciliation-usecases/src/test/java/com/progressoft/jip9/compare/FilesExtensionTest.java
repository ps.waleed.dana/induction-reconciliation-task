package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FilesExtensionTest {

    @Test
    public void givenFilesExtension_whenRequestIncorrectValue_thenFail(){
        Assertions.assertThrows(IllegalArgumentException.class, ()-> FilesExtension.parse("RRR"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> FilesExtension.parse("rrr"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> FilesExtension.parse("Rrr"));
    }

    @Test
    public void givenFilesExtension_whenRequestCorrectValues_thenWorkAsExpected(){
        Assertions.assertEquals(FilesExtension.CSV, FilesExtension.parse("csv"));
        Assertions.assertEquals(FilesExtension.CSV, FilesExtension.parse("Csv"));
        Assertions.assertEquals(FilesExtension.CSV, FilesExtension.parse("CSV"));
        Assertions.assertEquals(FilesExtension.JSON, FilesExtension.parse("json"));
        Assertions.assertEquals(FilesExtension.JSON, FilesExtension.parse("Json"));
        Assertions.assertEquals(FilesExtension.JSON, FilesExtension.parse("JSON"));
    }
}
