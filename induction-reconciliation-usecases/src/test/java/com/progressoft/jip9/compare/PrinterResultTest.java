package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.result.PrinterResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PrinterResultTest {

    @Test
    public void givenNullMatchPathToFile_whenRead_thenFail() {
        Path match = null;
        Path mismatch = Paths.get("..", "sample-files", "result-files", "mismatched-transactions.csv");
        Path missing = Paths.get("..", "sample-files", "result-files", "missing-transactions.csv");
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("null matching path", thrown.getMessage());
    }

    @Test
    public void givenNullMismatchPathToFile_whenRead_thenFail() {
        Path match = Paths.get("..", "sample-files", "result-files", "matching-transactions.csv");
        Path mismatch = null;
        Path missing = Paths.get("..", "sample-files", "result-files", "missing-transactions.csv");
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("null mismatched path", thrown.getMessage());
    }

    @Test
    public void givenNullMissingPathToFile_whenRead_thenFail() {
        Path match = Paths.get("..", "sample-files", "result-files", "matching-transactions.csv");
        Path mismatch = Paths.get("..", "sample-files", "result-files", "mismatched-transactions.csv");
        Path missing = null;
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("null missing path", thrown.getMessage());
    }

    @Test
    public void givenNotExistsMatchPath_whenRead_thenFail() {
        Path match = Paths.get("..", "sample-files" + "result-files", "match.csv");
        Path mismatch = Paths.get("..", "sample-files", "result-files", "mismatched-transactions.csv");
        Path missing = Paths.get("..", "sample-files", "result-files", "missing-transactions.csv");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("matching path does not exists", thrown.getMessage());
    }

    @Test
    public void givenNotExistsMismatchPath_whenRead_thenFail() {
        Path match = Paths.get("..", "sample-files" , "result-files", "matching-transactions.csv");
        Path mismatch = Paths.get("..", "sample-files", "result-files", "mismatch.csv");
        Path missing = Paths.get("..", "sample-files", "result-files", "missing-transactions.csv");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("mismatched path does not exists", thrown.getMessage());
    }

    @Test
    public void givenNotExistsMissingPath_whenRead_thenFail() {
        Path match = Paths.get("..", "sample-files" , "result-files", "matching-transactions.csv");
        Path mismatch = Paths.get("..", "sample-files", "result-files", "mismatched-transactions.csv");
        Path missing = Paths.get("..", "sample-files", "result-files", "missing.csv");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("missing path does not exists", thrown.getMessage());
    }

    @Test
    public void givenValidDirectoryMatchPath_whenRead_thenFail() throws IOException {
        Path match = Files.createTempDirectory("match-transactions");
        Path mismatch = Paths.get("..", "sample-files", "result-files", "mismatched-transactions.csv");
        Path missing = Paths.get("..", "sample-files", "result-files", "missing-transactions.csv");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("matching path is not a file", thrown.getMessage());
    }

    @Test
    public void givenValidDirectoryMismatchPath_whenRead_thenFail() throws IOException {
        Path match = Paths.get("..", "sample-files" , "result-files", "matching-transactions.csv");
        Path mismatch = Files.createTempDirectory("mismatch-transactions");
        Path missing = Paths.get("..", "sample-files", "result-files", "missing-transactions.csv");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("mismatched path is not a file", thrown.getMessage());
    }

    @Test
    public void givenValidDirectoryMissingPath_whenRead_thenFail() throws IOException {
        Path match = Paths.get("..", "sample-files" , "result-files", "matching-transactions.csv");
        Path mismatch = Paths.get("..", "sample-files", "result-files", "mismatched-transactions.csv");
        Path missing = Files.createTempDirectory("missing-transactions");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PrinterResult(match, mismatch, missing));
        Assertions.assertEquals("missing path is not a file", thrown.getMessage());
    }

    @Test
    public void givenValidPathResult_whenGetResultPath_thenReturnAsExpected() {
        Path match = Paths.get("..", "sample-files" , "result-files", "matching-transactions.csv");
        Path mismatch = Paths.get("..", "sample-files", "result-files", "mismatched-transactions.csv");
        Path missing = Paths.get("..", "sample-files", "result-files", "missing-transactions.csv");

        PrinterResult PrinterResult = new PrinterResult(match, mismatch, missing);

        Assertions.assertEquals(match, PrinterResult.getMatching());
        Assertions.assertEquals(mismatch, PrinterResult.getMismatching());
        Assertions.assertEquals(missing, PrinterResult.getMissing());
    }
}
