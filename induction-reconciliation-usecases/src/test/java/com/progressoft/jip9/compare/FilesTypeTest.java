package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FilesTypeTest {

    @Test
    public void givenFilesType_whenRequestIncorrectValue_thenFail(){
        Assertions.assertThrows(IllegalArgumentException.class, ()-> FilesType.parse("RRR"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> FilesType.parse("rrr"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> FilesType.parse("Rrr"));
    }

    @Test
    public void givenFilesType_whenRequestCorrectValues_thenWorkAsExpected(){
        Assertions.assertEquals(FilesType.SOURCE, FilesType.parse("source"));
        Assertions.assertEquals(FilesType.SOURCE, FilesType.parse("Source"));
        Assertions.assertEquals(FilesType.SOURCE, FilesType.parse("SOURCE"));
        Assertions.assertEquals(FilesType.TARGET, FilesType.parse("target"));
        Assertions.assertEquals(FilesType.TARGET, FilesType.parse("Target"));
        Assertions.assertEquals(FilesType.TARGET, FilesType.parse("TARGET"));
    }
}
