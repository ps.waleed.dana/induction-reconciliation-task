package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.result.CompareResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

public class CompareResultTest {

    @Test
    public void givenCompareResult_whenConstructNullMatchParameterList_thenFail() {
        List<Transaction> match = null;
        List<Transaction> mismatch = new LinkedList<>();
        List<Transaction> missing = new LinkedList<>();
        NullPointerException the_source_list_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> new CompareResult(match, mismatch, missing), "the source list is null");
        Assertions.assertEquals("the match list is null", the_source_list_is_null.getMessage());
    }

    @Test
    public void givenCompareResult_whenConstructNullMismatchParameterList_thenFail() {
        List<Transaction> match = new LinkedList<>();
        List<Transaction> mismatch = null;
        List<Transaction> missing = new LinkedList<>();
        NullPointerException the_source_list_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> new CompareResult(match, mismatch, missing), "the source list is null");
        Assertions.assertEquals("the mismatch list is null", the_source_list_is_null.getMessage());
    }

    @Test
    public void givenCompareResult_whenConstructNullMissingParameterList_thenFail() {
        List<Transaction> match = new LinkedList<>();
        List<Transaction> mismatch = new LinkedList<>();
        List<Transaction> missing = null;
        NullPointerException the_source_list_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> new CompareResult(match, mismatch, missing), "the source list is null");
        Assertions.assertEquals("the missing list is null", the_source_list_is_null.getMessage());
    }


    @Test
    public void givenReaderResult_whenWhenConstructParametersListNotNull_thenCanGetTheseLists() {
        List<Transaction> match = new LinkedList<>();
        List<Transaction> mismatch = new LinkedList<>();
        List<Transaction> missing = new LinkedList<>();
        CompareResult result = new CompareResult(match, mismatch, missing);

        Assertions.assertEquals(match, result.getMatch());
        Assertions.assertEquals(mismatch, result.getMismatch());
        Assertions.assertEquals(missing, result.getMissing());
    }
}
