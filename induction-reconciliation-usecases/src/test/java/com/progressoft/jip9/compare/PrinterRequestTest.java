package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.request.PrinterRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PrinterRequestTest {

    @Test
    public void givenPrinterRequest_whenConstructNullDirectory_thenFail() {
        String directory = null;
        String match = "/path.csv";
        String mismatch = "/path.csv";
        String missing = "/path.csv";
        FilesExtension filesExtension = FilesExtension.CSV;

        NullPointerException the_source_path_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> new PrinterRequest(directory, match, mismatch, missing, filesExtension), "the path is null");
        Assertions.assertEquals("directory path is null", the_source_path_is_null.getMessage());
    }

    @Test
    public void givenPrinterRequest_whenConstructNullMatch_thenFail() {
        String directory = "/";
        String match = null;
        String mismatch = "/path.csv";;
        String missing = "/path.csv";;
        FilesExtension filesExtension = FilesExtension.CSV;

        NullPointerException the_source_path_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> new PrinterRequest(directory, match, mismatch, missing, filesExtension), "the path is null");
        Assertions.assertEquals("match path is null", the_source_path_is_null.getMessage());

    }

    @Test
    public void givenPrinterRequest_whenConstructNullMismatch_thenFail() {
        String directory = "/";
        String match = "/path.csv";;
        String mismatch = null;
        String missing = "/path.csv";;
        FilesExtension filesExtension = FilesExtension.CSV;

        NullPointerException the_source_path_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> new PrinterRequest(directory, match, mismatch, missing, filesExtension), "the path is null");
        Assertions.assertEquals("mismatch path is null", the_source_path_is_null.getMessage());
    }

    @Test
    public void givenPrinterRequest_whenConstructNullMissing_thenFail() {
        String directory = "/";
        String match = "/path.csv";;
        String mismatch = "/path.csv";;
        String missing = null;
        FilesExtension filesExtension = FilesExtension.CSV;

        NullPointerException the_source_path_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> new PrinterRequest(directory, match, mismatch, missing, filesExtension), "the path is null");
        Assertions.assertEquals("missing path is null", the_source_path_is_null.getMessage());
    }

    @Test
    public void givenPrinterRequest_whenConstructNullFileExtension_thenFail() {
        String directory = "/";
        String match = "/path.csv";;
        String mismatch = "/path.csv";;
        String missing = "/path.csv";;
        FilesExtension filesExtension = null;

        NullPointerException fileExtensionIsNull = Assertions.assertThrows(NullPointerException.class,
                () -> new PrinterRequest(directory, match, mismatch, missing, filesExtension), "file extension is null");
        Assertions.assertEquals("file extension is null", fileExtensionIsNull.getMessage());
    }

    @Test
    public void givenPrinterRequest_whenWhenConstructParametersNotNull_thenCanGetThesePaths() {
        Path directory = Paths.get("/");
        Path match = Paths.get("/", "match.csv");
        Path mismatch = Paths.get("/", "mismatch.csv");
        Path missing = Paths.get("/", "missing.csv");
        FilesExtension filesExtension = FilesExtension.CSV;
        PrinterRequest request = new PrinterRequest(directory.toString(), match.toString(),
                mismatch.toString(), missing.toString(), filesExtension);

        Assertions.assertEquals(directory, request.getDirectory());
        Assertions.assertEquals(match, request.getMatchPath());
        Assertions.assertEquals(mismatch, request.getMismatchPath());
        Assertions.assertEquals(missing, request.getMissingPath());
    }
}