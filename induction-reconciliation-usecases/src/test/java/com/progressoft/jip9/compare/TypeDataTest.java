package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.TypeData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TypeDataTest {

    @Test
    public void givenTypeData_whenRequestIncorrectValue_thenFail(){
        Assertions.assertThrows(IllegalArgumentException.class, ()-> TypeData.parse("RRR"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> TypeData.parse("rrr"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> TypeData.parse("Rrr"));
    }

    @Test
    public void givenTypeData_whenRequestCorrectValues_thenWorkAsExpected(){
        Assertions.assertEquals(TypeData.MATCH, TypeData.parse("match"));
        Assertions.assertEquals(TypeData.MATCH, TypeData.parse("Match"));
        Assertions.assertEquals(TypeData.MATCH, TypeData.parse("MATCh"));
        Assertions.assertEquals(TypeData.MISMATCH, TypeData.parse("mismatch"));
        Assertions.assertEquals(TypeData.MISMATCH, TypeData.parse("Mismatch"));
        Assertions.assertEquals(TypeData.MISMATCH, TypeData.parse("MISMATCH"));
        Assertions.assertEquals(TypeData.MISSING, TypeData.parse("missing"));
        Assertions.assertEquals(TypeData.MISSING, TypeData.parse("Missing"));
        Assertions.assertEquals(TypeData.MISSING, TypeData.parse("MISSING"));
    }
}
