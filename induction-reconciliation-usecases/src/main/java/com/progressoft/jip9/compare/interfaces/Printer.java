package com.progressoft.jip9.compare.interfaces;

import com.progressoft.jip9.compare.request.CreateFileRequest;
import com.progressoft.jip9.compare.request.PrinterRequest;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.PrinterResult;

public interface Printer {

    PrinterResult print(CompareResult result, PrinterRequest printerRequest);
    void print (CreateFileRequest request);
}