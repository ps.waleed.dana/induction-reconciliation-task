package com.progressoft.jip9.compare.interfaces;

import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.ReaderResult;

public interface Comparator {

    CompareResult compare(ReaderResult source, ReaderResult target);

}
