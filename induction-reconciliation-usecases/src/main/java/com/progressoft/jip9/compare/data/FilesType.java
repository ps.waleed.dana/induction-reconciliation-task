package com.progressoft.jip9.compare.data;

import java.util.HashMap;
import java.util.Map;

public enum  FilesType {
    SOURCE,TARGET;

    private static Map<String, FilesType> roles;
    private static String message;

    static {
        defineRoles();
        defineMessage();
    }

    private static void defineRoles() {
        roles = new HashMap<>();
        for (FilesType r : FilesType.values()) {
            roles.put(r.toString(), r);
        }
    }

    private static void defineMessage() {
        FilesType[] values = FilesType.values();
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append("Your file type should be ");
        for (int i = 0; i < values.length; i++) {
            if (i == values.length - 1)
                messageBuilder.append(values[i]);
            else
                messageBuilder.append(values[i]).append(" or ");
        }
        message = messageBuilder.toString();
    }

    public static FilesType parse(String s) {
        if (roles.get(s.toUpperCase()) == null)
            throw new IllegalArgumentException(message);
        return roles.get(s.toUpperCase());
    }
}
