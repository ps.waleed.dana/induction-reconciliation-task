package com.progressoft.jip9.compare.result;

import java.nio.file.Files;
import java.nio.file.Path;

public class PrinterResult {

    private final Path matching;
    private final Path mismatching;
    private final Path missing;

    public PrinterResult(Path matching, Path mismatching, Path missing) {
        validateFiles(matching, "null matching path", "matching path does not exists", "matching path is not a file");
        validateFiles(mismatching, "null mismatched path", "mismatched path does not exists", "mismatched path is not a file");
        validateFiles(missing, "null missing path", "missing path does not exists", "missing path is not a file");

        this.matching = matching;
        this.mismatching = mismatching;
        this.missing = missing;
    }

    private void validateFiles(Path file, String nullMessage, String notExistsMessage, String notFileMessage) {
        if (file == null)
            throw new NullPointerException(nullMessage);
        if (Files.notExists(file))
            throw new IllegalArgumentException(notExistsMessage);
        if (Files.isDirectory(file))
            throw new IllegalArgumentException(notFileMessage);
    }

    public Path getMismatching() {
        return mismatching;
    }

    public Path getMissing() {
        return missing;
    }

    public Path getMatching() {
        return matching;
    }
}
