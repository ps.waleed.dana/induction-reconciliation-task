package com.progressoft.jip9.compare.interfaces;

import com.progressoft.jip9.compare.request.ReaderRequest;
import com.progressoft.jip9.compare.result.ReaderResult;

public interface Reader {

    ReaderResult read(ReaderRequest request);
}
