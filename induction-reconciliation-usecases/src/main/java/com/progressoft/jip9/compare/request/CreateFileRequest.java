package com.progressoft.jip9.compare.request;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.data.TypeData;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

public class CreateFileRequest {
    private final List<Transaction> list;
    private final Path path;
    private final FilesExtension filesExtension;
    private final TypeData typeData;

    public CreateFileRequest(List<Transaction> list, Path path, FilesExtension filesExtension, TypeData typeData) {
        if (list == null)
            throw new NullPointerException("the list is null");
        if (path == null)
            throw new NullPointerException("the path is null");
        if (filesExtension == null)
            throw new NullPointerException("the file extension is null");
        if (typeData == null)
            throw new NullPointerException("the type data is null");

        this.list = list;
        this.path = path;
        this.filesExtension = filesExtension;
        this.typeData = typeData;
    }

    public List<Transaction> getList() {
        return Collections.unmodifiableList(list);
    }

    public Path getPath() {
        return path;
    }

    public FilesExtension getFilesExtension() {
        return filesExtension;
    }

    public TypeData getTypeData() {
        return typeData;
    }
}
