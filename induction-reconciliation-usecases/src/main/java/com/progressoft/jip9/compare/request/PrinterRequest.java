package com.progressoft.jip9.compare.request;

import com.progressoft.jip9.compare.data.FilesExtension;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PrinterRequest {

    private final Path directory;
    private final Path matchPath;
    private final Path mismatchPath;
    private final Path missingPath;
    private final FilesExtension filesExtension;

    public PrinterRequest(String directory, String matchPath, String mismatchPath,
                          String missingPath, FilesExtension filesExtension) {
        validatePaths(directory, "directory path is null");
        validatePaths(matchPath, "match path is null");
        validatePaths(mismatchPath, "mismatch path is null");
        validatePaths(missingPath, "missing path is null");
        validateFileExtension(filesExtension);

        this.directory = Paths.get(directory);
        this.filesExtension = filesExtension;
        this.matchPath = Paths.get(directory, matchPath);
        this.mismatchPath = Paths.get(directory, mismatchPath);
        this.missingPath = Paths.get(directory, missingPath);
    }

    public Path getDirectory() {
        return directory;
    }

    public Path getMatchPath() {
        return matchPath;
    }

    public Path getMismatchPath() {
        return mismatchPath;
    }

    public Path getMissingPath() {
        return missingPath;
    }

    public FilesExtension getFileExtension() {
        return filesExtension;
    }

    private void validateFileExtension(FilesExtension fileExtension) {
        if (fileExtension == null)
            throw new NullPointerException("file extension is null");
    }

    private void validatePaths(String directory, String s) {
        if (directory == null)
            throw new NullPointerException(s);
    }
}
