package com.progressoft.jip9.compare.modul;

import com.progressoft.jip9.compare.data.FilesType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Transaction implements Serializable {

    private String reference;
    private BigDecimal amount;
    private String currencyCode;
    private LocalDate date;
    private String transDescription;
    private String purpose;
    private String transType;
    private FilesType foundInFile;

    public Transaction(String reference, String transDescription, String amount,
                       String currencyCode, String purpose, LocalDate date,
                       String transType, FilesType foundInFile) {

        this.reference = reference;
        this.transDescription = transDescription;
        this.purpose = purpose;
        this.transType = transType;
        this.currencyCode = currencyCode;
        this.amount = new BigDecimal(amount);
        this.foundInFile = foundInFile;
        this.date = date;
    }

    public Transaction() {
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setDate(String date) {
        String[] split = date.split("/");
        String finalDate = split[2] + "-" + split[1] + "-" + split[0];
        this.date = LocalDate.parse(finalDate);
    }

    public void setTransDescription(String transDescription) {
        this.transDescription = transDescription;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public void setFoundInFile(FilesType foundInFile) {
        this.foundInFile = foundInFile;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getDate() {
        return date.toString();
    }

    public String getReference() {
        return reference;
    }

    public String getTransDescription() {
        return transDescription;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getTransType() {
        return transType;
    }

    public FilesType getFoundInFile() {
        return foundInFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Transaction that = (Transaction) o;
        return amount.compareTo(that.amount) == 0 &&
                Objects.equals(currencyCode, that.currencyCode) &&
                Objects.equals(reference, that.reference) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, currencyCode, date);
    }

}