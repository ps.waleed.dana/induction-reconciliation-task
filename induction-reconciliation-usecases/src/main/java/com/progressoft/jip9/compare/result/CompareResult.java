package com.progressoft.jip9.compare.result;

import com.progressoft.jip9.compare.modul.Transaction;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CompareResult implements Serializable {

    private final List<Transaction> match;
    private final List<Transaction> mismatch;
    private final List<Transaction> missing;

    public CompareResult(List<Transaction> match, List<Transaction> mismatch, List<Transaction> missing) {
        if (match == null)
            throw new NullPointerException("the match list is null");
        if (mismatch == null)
            throw new NullPointerException("the mismatch list is null");
        if (missing == null)
            throw new NullPointerException("the missing list is null");

        this.match = match;
        this.mismatch = mismatch;
        this.missing = missing;
    }

    public int getMatchSize(){
        return match.size();
    }

    public int getMismatchSize(){
        return mismatch.size();
    }

    public int getMissingSize(){
        return missing.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        CompareResult that = (CompareResult) o;
        return Objects.equals(match, that.match) &&
                Objects.equals(mismatch, that.mismatch) &&
                Objects.equals(missing, that.missing);
    }

    @Override
    public int hashCode() {
        return Objects.hash(match, mismatch, missing);
    }

    public List<Transaction> getMatch() {
        return Collections.unmodifiableList(match);
    }

    public List<Transaction> getMismatch() {
        return Collections.unmodifiableList(mismatch);
    }

    public List<Transaction> getMissing() {
        return Collections.unmodifiableList(missing);
    }
}
