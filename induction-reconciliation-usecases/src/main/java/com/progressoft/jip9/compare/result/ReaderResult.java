package com.progressoft.jip9.compare.result;

import com.progressoft.jip9.compare.modul.Transaction;

import java.util.LinkedList;
import java.util.List;

public class ReaderResult {
    private final List<Transaction> list;

    public ReaderResult(List<Transaction> list) {
        if (list == null)
            throw new NullPointerException("the list is null");
        this.list = list;
    }

    public List<Transaction> getList() {
        return new LinkedList<>(list);
    }
}
