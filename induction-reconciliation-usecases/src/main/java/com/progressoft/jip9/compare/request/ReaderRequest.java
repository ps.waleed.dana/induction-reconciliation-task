package com.progressoft.jip9.compare.request;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.FilesType;

import java.nio.file.Files;
import java.nio.file.Path;

public class ReaderRequest {

    private final Path path;
    private final FilesExtension extension;
    private final FilesType type;

    public ReaderRequest(Path path, FilesExtension extension, FilesType type) {
        if (path == null) {
            throw new NullPointerException("null path");
        }
        if (Files.notExists(path)) {
            throw new IllegalArgumentException("path does not exists");
        }
        if (Files.isDirectory(path)) {
            throw new IllegalArgumentException("path is not a file");
        }
        if (extension == null) {
            throw new NullPointerException("null extension file");
        }
        if (type == null) {
            throw new NullPointerException("null type file");
        }
        this.type = type;
        this.path = path;
        this.extension = extension;
    }

    public Path getPath() {
        return path;
    }

    public FilesExtension getExtension() {
        return extension;
    }

    public FilesType getType() {
        return type;
    }
}
