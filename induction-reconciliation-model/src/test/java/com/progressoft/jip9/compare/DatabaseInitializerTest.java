package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

public class DatabaseInitializerTest {

    @Disabled
    @Test
    public void givenDatabaseInitializer_whenCreateTablesUsersAndReconciliations_thenReturnAsExpected(){
        // This test case doesn't work with h2 database
        // because when i try to get tables from metadata class with h2
        // not working as expected but in mysql case works fine
        DataSource dataSource = DatabaseInitializer.getH2DataSource();
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(dataSource);

        Assertions.assertTrue(databaseInitializer.createTableUsersIfNotExists());
        Assertions.assertFalse(databaseInitializer.createTableUsersIfNotExists());
        Assertions.assertTrue(databaseInitializer.createTableReconciliationsIfNotExists());
        Assertions.assertFalse(databaseInitializer.createTableReconciliationsIfNotExists());
    }
}