package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.implement.DBReconciliationsProcessRepository;
import com.progressoft.jip9.compare.implement.DBUsersProcessRepository;
import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.ReconciliationsProcess;
import com.progressoft.jip9.compare.interfaces.UsersProcess;
import com.progressoft.jip9.compare.modul.Reconciliation;
import com.progressoft.jip9.compare.modul.User;
import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ReconciliationsProcessTest {

    private ReconciliationsProcess reconciliationsProcess;
    private UsersProcess usersProcess;

    @BeforeEach
    public void setup() {
        DataSource dataSource = DatabaseInitializer.getH2DataSource();
        DatabaseInitializer initializer = new DatabaseInitializer(dataSource);
        reconciliationsProcess = new DBReconciliationsProcessRepository(initializer, dataSource);
        usersProcess = new DBUsersProcessRepository(initializer, dataSource);
        reconciliationsProcess.deleteAllReconciliations();
        usersProcess.deleteAllUsers();
    }

    @Test
    public void givenReconciliationsProcess_whenDoReconciliationProcess_thenSuccess() throws SQLException {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        User user = new User("admin", "admin");
        usersProcess.addUser(user);
        LocalDateTime now = LocalDateTime.now();
        Reconciliation reconciliation = new Reconciliation(1, now, now, "waleed"
        , "mohammad", 3, 4, 3);

        reconciliationsProcess.addReconciliation(reconciliation);
        List<String> sessionsTime = reconciliationsProcess.getSessionsTime(1);
        Assertions.assertEquals(now.format(dateTimeFormatter), sessionsTime.get(0));
        List<Reconciliation> reconciliations = reconciliationsProcess.getReconciliations(1, sessionsTime.get(0));
        Assertions.assertEquals(reconciliation, reconciliations.get(0));
        reconciliationsProcess.deleteAllReconciliations();

        List<String> sessionsTime1 = reconciliationsProcess.getSessionsTime(1);
        Assertions.assertTrue(sessionsTime1.isEmpty());

    }
}
