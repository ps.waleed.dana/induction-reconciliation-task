package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.implement.DBReconciliationsProcessRepository;
import com.progressoft.jip9.compare.implement.DBUsersProcessRepository;
import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.ReconciliationsProcess;
import com.progressoft.jip9.compare.interfaces.UsersProcess;
import com.progressoft.jip9.compare.modul.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

public class UsersProcessTest {

    private UsersProcess usersProcess;
    private ReconciliationsProcess reconciliationsProcess;

    @BeforeEach
    public void setup() {
        DataSource dataSource = DatabaseInitializer.getH2DataSource();
        DatabaseInitializer initializer = new DatabaseInitializer(dataSource);
        usersProcess = new DBUsersProcessRepository(initializer, dataSource);
        reconciliationsProcess = new DBReconciliationsProcessRepository(initializer, dataSource);
        reconciliationsProcess.deleteAllReconciliations();
        usersProcess.deleteAllUsers();
    }

    @Test
    public void givenUsersProcess_whenDoUsersProcess_thenSuccess(){
        User user = new User("admin", "admin");
        usersProcess.addUser(user);
        int login = usersProcess.login(user);
        Assertions.assertEquals(1, login);

        Assertions.assertTrue(usersProcess.isUsernameExists("admin"));
        usersProcess.deleteAllUsers();
        Assertions.assertFalse(usersProcess.isUsernameExists("admin"));
    }
}
