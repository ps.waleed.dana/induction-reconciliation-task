package com.progressoft.jip9.compare.interfaces;

import com.progressoft.jip9.compare.modul.Reconciliation;

import java.util.List;

public interface ReconciliationsProcess {

    void addReconciliation(Reconciliation reconciliation);
    List<Reconciliation> getReconciliations(int userId, String sessionTime);
    List<String> getSessionsTime(int userId);
    void deleteAllReconciliations();
}
