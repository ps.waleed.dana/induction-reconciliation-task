package com.progressoft.jip9.compare.interfaces;

import com.progressoft.jip9.compare.modul.User;

public interface UsersProcess {

    boolean addUser(User user);

    int login(User user);

    void deleteAllUsers();

    boolean isUsernameExists(String username);

}
