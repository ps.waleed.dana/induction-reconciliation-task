package com.progressoft.jip9.compare.implement;

import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.ReconciliationsProcess;
import com.progressoft.jip9.compare.modul.Reconciliation;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DBReconciliationsProcessRepository implements ReconciliationsProcess {

    private static final String ADD_NEW_RECONCILIATION = "insert into reconciliations " +
            "(id, session_time, time_rec, source_name," +
            " target_name, match_count, mismatch_count, missing_count)" +
            "values (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String GET_SESSIONS_TIME = "select session_time from reconciliations where id = ? limit 1";
    private static final String GET_RECONCILIATION_BASHED_ON_SESSION_TIME =
            "select * from reconciliations where id = ? and session_time = ?";
    public static final String DELETE_ALL_RECONCILIATIONS = "delete from reconciliations";

    private DatabaseInitializer initializer;
    private DataSource dataSource;

    public DBReconciliationsProcessRepository(DatabaseInitializer initializer, DataSource dataSource) {
        if (initializer == null)
            throw new NullPointerException("null initializer");
        this.initializer = initializer;
        this.dataSource = dataSource;
    }

    @Override
    public void addReconciliation(Reconciliation reconciliation) {
        if (reconciliation == null)
            throw new NullPointerException("null reconciliation");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(ADD_NEW_RECONCILIATION)) {
                setParameterAddReconciliation(reconciliation, dateTimeFormatter, statement);
                statement.executeUpdate();
            }
        } catch (SQLSyntaxErrorException e) {
            // TODO this should be part of your initializer
            if (initializer.createTableReconciliationsIfNotExists()) {
                addReconciliation(reconciliation);
                return;
            }
            throw new IllegalArgumentException(e);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Reconciliation> getReconciliations(int userId, String sessionTime) {
        List<Reconciliation> reconciliationList = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(GET_RECONCILIATION_BASHED_ON_SESSION_TIME)) {
                preparedStatement.setInt(1, userId);
                preparedStatement.setString(2, sessionTime);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        reconciliationList.add(
                                getReconciliationObject(userId, resultSet));
                    }
                    return reconciliationList;
                }
            }
        } catch (SQLSyntaxErrorException e) {
            if (initializer.createTableReconciliationsIfNotExists())
                return reconciliationList;
            throw new IllegalArgumentException(e);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<String> getSessionsTime(int userId) {
        List<String> sessionsTimeList = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(GET_SESSIONS_TIME)) {
                preparedStatement.setInt(1, userId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        sessionsTimeList.add(resultSet.getString("session_time"));
                    }
                    return sessionsTimeList;
                }
            }
        } catch (SQLSyntaxErrorException e) {
            if (initializer.createTableReconciliationsIfNotExists())
                return sessionsTimeList;
            throw new IllegalArgumentException(e);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteAllReconciliations() {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(DELETE_ALL_RECONCILIATIONS);
            }
        } catch (SQLSyntaxErrorException e) {
            if (initializer.createTableReconciliationsIfNotExists())
                return;
            throw new IllegalArgumentException(e);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Reconciliation getReconciliationObject(int userId, ResultSet resultSet) throws SQLException {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return new Reconciliation(
                userId,
                LocalDateTime.parse(resultSet.getString("session_time"), dateTimeFormatter),
                LocalDateTime.parse(resultSet.getString("time_rec"), dateTimeFormatter),
                resultSet.getString("source_name"),
                resultSet.getString("target_name"),
                resultSet.getInt("match_count"),
                resultSet.getInt("mismatch_count"),
                resultSet.getInt("missing_count"));
    }

    private void setParameterAddReconciliation(Reconciliation reconciliation, DateTimeFormatter dateTimeFormatter, PreparedStatement statement) throws SQLException {
        statement.setInt(1, reconciliation.getUserId());
        statement.setString(2, reconciliation.getSessionTime().format(dateTimeFormatter));
        statement.setString(3, reconciliation.getReconciliationTime().format(dateTimeFormatter));
        statement.setString(4, reconciliation.getSourceName());
        statement.setString(5, reconciliation.getTargetName());
        statement.setInt(6, reconciliation.getMatchCount());
        statement.setInt(7, reconciliation.getMismatchCount());
        statement.setInt(8, reconciliation.getMissingCount());
    }
}