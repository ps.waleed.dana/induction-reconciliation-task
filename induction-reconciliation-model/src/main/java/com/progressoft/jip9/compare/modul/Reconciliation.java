package com.progressoft.jip9.compare.modul;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Reconciliation {

    private final int userId;
    private final LocalDateTime sessionTime;
    private final LocalDateTime reconciliationTime;
    private final String sourceName;
    private final String targetName;
    private final int matchCount;
    private final int mismatchCount;
    private final int missingCount;

    public Reconciliation(int userId, LocalDateTime sessionTime, LocalDateTime reconciliationTime, String sourceName,
                          String targetName, int matchCount, int mismatchCount, int missingCount) {
        this.userId = userId;
        this.sessionTime = sessionTime;
        this.reconciliationTime = reconciliationTime;
        this.sourceName = sourceName;
        this.targetName = targetName;
        this.matchCount = matchCount;
        this.mismatchCount = mismatchCount;
        this.missingCount = missingCount;
    }

    public LocalDateTime getSessionTime() {
        return sessionTime;
    }

    public LocalDateTime getReconciliationTime() {
        return reconciliationTime;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getTargetName() {
        return targetName;
    }

    public int getMatchCount() {
        return matchCount;
    }

    public int getMismatchCount() {
        return mismatchCount;
    }

    public int getMissingCount() {
        return missingCount;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Reconciliation that = (Reconciliation) o;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return userId == that.userId &&
                matchCount == that.matchCount &&
                mismatchCount == that.mismatchCount &&
                missingCount == that.missingCount &&
                Objects.equals(sessionTime.format(dateTimeFormatter), that.sessionTime.format(dateTimeFormatter)) &&
                Objects.equals(reconciliationTime.format(dateTimeFormatter), that.reconciliationTime.format(dateTimeFormatter)) &&
                Objects.equals(sourceName, that.sourceName) &&
                Objects.equals(targetName, that.targetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash
                (userId, sessionTime, reconciliationTime, sourceName,
                        targetName, matchCount, mismatchCount, missingCount);
    }
}
