package com.progressoft.jip9.compare.initialize;

import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;
import java.sql.*;

public class DatabaseInitializer {

    private static final String CREATE_TABLE_USERS = "create table users(id INTEGER not null AUTO_INCREMENT ,username varchar(20) unique not null ,password varchar (200) not null,primary key (id))";
    private static final String CREATE_TABLE_RECONCILIATION = "create table reconciliations(id INTEGER not null,session_time varchar (20),time_rec varchar (20),source_name varchar (20),target_name varchar (20),match_count INTEGER,mismatch_count INTEGER,missing_count INTEGER,    FOREIGN KEY (id) REFERENCES users(id))";
    private final DataSource dataSource;

    public DatabaseInitializer(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static DataSource getMysqlDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl("jdbc:mysql://localhost:3306/reconciliation?createDatabaseIfNotExist=true&serverTimezone=UTC");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        return dataSource;
    }

    public static DataSource getH2DataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl("jdbc:h2:mem:reconciliation");
        dataSource.setDriverClassName("org.h2.Driver");
        return dataSource;
    }

    public boolean createTableReconciliationsIfNotExists() {

        String tableName = "reconciliations";

        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();

            if (isTableNotFound(metaData, tableName)) {
                createTableUsersIfNotExists();
                return createTable(CREATE_TABLE_RECONCILIATION, connection);
            }
            return false;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public boolean createTableUsersIfNotExists() {
        String tableName = "users";

        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            if (isTableNotFound(metaData, tableName))
                return createTable(CREATE_TABLE_USERS, connection);
            return false;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    // This method work fine with mysql but doesn't work as expected with h2 database
    private boolean isTableNotFound(DatabaseMetaData metaData, String tableName) throws SQLException {
        try (ResultSet rs = metaData.getTables(null, null, tableName,
                new String[]{"TABLE"})) {
            return !rs.next();
        }
    }

    private boolean createTable(String createTable, Connection connection) {
        try (Statement statement = connection.createStatement()) {
            statement.execute(createTable);
            return true;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
