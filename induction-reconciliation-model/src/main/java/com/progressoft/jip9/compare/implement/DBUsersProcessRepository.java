package com.progressoft.jip9.compare.implement;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.UsersProcess;
import com.progressoft.jip9.compare.modul.User;
import javax.sql.DataSource;
import java.sql.*;

public class DBUsersProcessRepository implements UsersProcess {

    public static final String ADD_NEW_USER = "insert into users (username, password) values (?,?)";
    public static final String LOGIN = "select * from users where username = ?";
    public static final String DELETE_ALL_USERS = "delete from users";
    public static final String RESET_AUTO_INCREMENT_ID = "ALTER TABLE users ALTER COLUMN id RESTART WITH 1";
    public static final String CHECK_IF_USERNAME_EXISTS_OR_NOT = "select count(*) as count from users where username = ?";
    private final DatabaseInitializer initializer;
    private final DataSource dataSource;

    public DBUsersProcessRepository(DatabaseInitializer initializer, DataSource dataSource) {
        if (initializer == null)
            throw new NullPointerException("null initializer");
        this.initializer = initializer;
        this.dataSource = dataSource;
    }

    @Override
    public boolean addUser(User user) {
        if (user == null)
            throw new NullPointerException("null user");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(ADD_NEW_USER)) {
                String password = BCrypt.withDefaults().hashToString(6, user.getPassword().toCharArray());
                statement.setString(1, user.getUsername());
                statement.setString(2, password);
                statement.executeUpdate();
            }
            return true;
        } catch (SQLSyntaxErrorException e) {
            if (initializer.createTableUsersIfNotExists())
                return addUser(user);
            throw new IllegalArgumentException(e);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public int login(User user) {
        if (user == null)
            throw new NullPointerException("null user");

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(LOGIN)) {
                statement.setString(1, user.getUsername());
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        return isEqualPassword(user, resultSet);
                    }
                    return 0;
                }
            }
        } catch (SQLSyntaxErrorException e) {
            if (initializer.createTableUsersIfNotExists())
                return login(user);
            throw new IllegalArgumentException(e);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteAllUsers() {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(DELETE_ALL_USERS);
                statement.executeUpdate(RESET_AUTO_INCREMENT_ID);
            }
        } catch (SQLSyntaxErrorException e) {
            if (initializer.createTableUsersIfNotExists())
                return;
            throw new IllegalArgumentException(e);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public boolean isUsernameExists(String username) {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(CHECK_IF_USERNAME_EXISTS_OR_NOT)) {
                statement.setString(1, username);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = Integer.parseInt(resultSet.getString("count"));
                        return count > 0;
                    }
                }
            }
            return false;
        } catch (SQLSyntaxErrorException e) {
            if (initializer.createTableUsersIfNotExists())
                return false;
            throw new IllegalArgumentException(e);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private int isEqualPassword(User user, ResultSet resultSet) throws SQLException {
        String password = resultSet.getString("password");
        BCrypt.Result verify = BCrypt.verifyer().verify(user.getPassword().toCharArray(), password);
        return verify.verified ? resultSet.getInt("id") : 0;
    }
}
