create table IF NOT EXISTS users(
id INTEGER not null AUTO_INCREMENT ,
username varchar(20) unique not null ,
password varchar (20) not null,
primary key (id)
);

create table IF NOT EXISTS reconciliations(
id INTEGER not null,
session_time varchar (20),
time_rec varchar (20),
source_name varchar (20),
target_name varchar (20),
match_count INTEGER,
mismatch_count INTEGER,
missing_count INTEGER,
    FOREIGN KEY (id) REFERENCES users(id)
);