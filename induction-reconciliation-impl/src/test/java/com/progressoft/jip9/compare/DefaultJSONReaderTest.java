package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.interfaces.Reader;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.request.ReaderRequest;
import com.progressoft.jip9.compare.result.ReaderResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DefaultJSONReaderTest {

    private Reader reader;

    @BeforeEach
    public void setup() {
        reader = new DefaultReader();
    }

    @Test
    public void canCreate() {
        new DefaultReader();
    }

    @Test
    public void givenReader_whenReadWithRequestNull_thenFail() {
        NullPointerException the_request_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> reader.read(null), "the request is null");
        Assertions.assertEquals("the request is null", the_request_is_null.getMessage());
    }

    @Test
    public void givenReader_whenReadWithCorrectRequest_thenReturnList() {
        Path path = Paths.get("..", "request", "online-banking-transactions.json");
        ReaderRequest request = new ReaderRequest(path, FilesExtension.JSON, FilesType.SOURCE);
        ReaderResult result = reader.read(request);
        List<Transaction> list = result.getList();
        Assertions.assertNotNull(list);
        Assertions.assertEquals("USD", list.get(0).getCurrencyCode());
        Assertions.assertEquals("donation", list.get(0).getPurpose());
        Assertions.assertEquals("2020-01-20", list.get(0).getDate().toString());
        Assertions.assertEquals("TR-47884222201", list.get(0).getReference());
        Assertions.assertEquals(new BigDecimal("140.00"), list.get(0).getAmount());
    }
}
