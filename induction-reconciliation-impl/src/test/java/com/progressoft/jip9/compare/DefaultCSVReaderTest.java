package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.interfaces.Reader;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.request.ReaderRequest;
import com.progressoft.jip9.compare.result.ReaderResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DefaultCSVReaderTest {
    private Reader reader;

    @BeforeEach
    public void setup() {
        reader = new DefaultReader();
    }

    @Test
    public void canCreate() {
        new DefaultReader();
    }

    @Test
    public void givenReader_whenCompareWithRequestNull_thenFail() {
        NullPointerException the_request_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> reader.read(null), "the request is null");
        Assertions.assertEquals("the request is null", the_request_is_null.getMessage());
    }

    @Test
    public void givenReader_whenReadWithCorrectRequest_thenReturnList(){
        Path path = Paths.get("..", "request", "bank-transactions.csv");
        ReaderRequest request = new ReaderRequest(path, FilesExtension.CSV, FilesType.SOURCE);
        ReaderResult result = reader.read(request);
        List<Transaction> list = result.getList();
        Assertions.assertNotNull(list);
        Assertions.assertEquals(list.get(0).getAmount(), new BigDecimal("140"));
        Assertions.assertEquals(list.get(0).getCurrencyCode(), "USD");
        Assertions.assertEquals(list.get(0).getTransDescription(), "online transfer");
        Assertions.assertEquals(list.get(0).getDate().toString(), "2020-01-20");
        Assertions.assertEquals(list.get(0).getReference(), "TR-47884222201");
    }
}
