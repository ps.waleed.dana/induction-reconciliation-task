package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.data.TypeData;
import com.progressoft.jip9.compare.interfaces.Printer;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.request.CreateFileRequest;
import com.progressoft.jip9.compare.request.PrinterRequest;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.PrinterResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class DefaultCSVPrinterTest {
    private Printer printer;

    @BeforeEach
    public void setup() {
        printer = new DefaultPrinter();
    }

    @Test
    public void canCreate() {
        new DefaultPrinter();
    }

    @Test
    public void givenPrinter_whenPrintWithRequestNull_thenFail() {
        NullPointerException the_request_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> printer.print(null, null), "the request is null");
        Assertions.assertEquals("the request is null", the_request_is_null.getMessage());
    }

    @Test
    public void givenPrinter_whenPrintWithCreateFileRequestNull_thenFail() {
        NullPointerException the_request_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> printer.print(null), "the request is null");
        Assertions.assertEquals("the request is null", the_request_is_null.getMessage());
    }

    @Test
    public void givenPrinter_whenPrint_thenReturnAsExpected() throws IOException {
        List<Transaction> match = new LinkedList<>();
        List<Transaction> mismatch = new LinkedList<>();
        List<Transaction> missing = new LinkedList<>();
        fillMatchList(match);
        fillMismatchAndMissingLists(mismatch, "124", "JOD", "2018-01-01", "124", "2018-01-05");
        fillMismatchAndMissingLists(missing, "125", "USD", "2018-03-01", "126", "2018-03-01");

        CompareResult result = new CompareResult(match, mismatch, missing);

        String directory = "/home/user/results";
        PrinterRequest printerRequest = new PrinterRequest(directory,
                "matching-transactions.csv",
                "mismatched-transactions.csv",
                "missing-transactions.csv", FilesExtension.CSV);

        PrinterResult print = printer.print(result, printerRequest);

        Assertions.assertNotNull(print.getMatching());
        Assertions.assertNotNull(print.getMismatching());
        Assertions.assertNotNull(print.getMissing());

        Assertions.assertFalse(Files.notExists(print.getMatching()));
        Assertions.assertFalse(Files.notExists(print.getMismatching()));
        Assertions.assertFalse(Files.notExists(print.getMissing()));

        Assertions.assertFalse(Files.isDirectory(print.getMatching()));
        Assertions.assertFalse(Files.isDirectory(print.getMismatching()));
        Assertions.assertFalse(Files.isDirectory(print.getMissing()));

        checkFiles(print.getMatching(), "transaction id,amount,currency code,value date", "123,1.00,SAR,2018-02-01");
        checkFiles(print.getMismatching(), "found in file,transaction id,amount,currency code,value date", "SOURCE,124,1.000,JOD,2018-01-01");
        checkFiles(print.getMissing(), "found in file,transaction id,amount,currency code,value date", "SOURCE,125,1.00,USD,2018-03-01");
    }

    @Test
    public void givenPrinter_whenPrintOneFileMatch_thenReturnAsExpected() throws IOException {
        List<Transaction> match = new LinkedList<>();
        fillMatchList(match);
        String file = "/home/user/results/matching-transactions.csv";
        Path path = Paths.get(file);

        CreateFileRequest createFileRequest = new CreateFileRequest(match,
                path, FilesExtension.CSV, TypeData.MATCH);

        printer.print(createFileRequest);

        Assertions.assertFalse(Files.notExists(path));
        Assertions.assertFalse(Files.isDirectory(path));

        checkFiles(path, "transaction id,amount,currency code,value date", "123,1.00,SAR,2018-02-01");
    }

    @Test
    public void givenPrinter_whenPrintOneFileMismatch_thenReturnAsExpected() throws IOException {
        List<Transaction> mismatch = new LinkedList<>();
        fillMismatchAndMissingLists(mismatch, "124", "JOD", "2018-01-01", "124", "2018-01-05");
        String file = "/home/user/results/mismatched-transactions.csv";
        Path path = Paths.get(file);

        CreateFileRequest createFileRequest = new CreateFileRequest(mismatch,
                path, FilesExtension.CSV, TypeData.MISMATCH);

        printer.print(createFileRequest);

        Assertions.assertFalse(Files.notExists(path));
        Assertions.assertFalse(Files.isDirectory(path));

        checkFiles(path, "found in file,transaction id,amount,currency code,value date", "SOURCE,124,1.000,JOD,2018-01-01");
    }

    @Test
    public void givenPrinter_whenPrintOneFileMissing_thenReturnAsExpected() throws IOException {
        List<Transaction> missing = new LinkedList<>();
        fillMismatchAndMissingLists(missing, "125", "USD", "2018-03-01", "126", "2018-03-01");
        String file = "/home/user/results/missing-transactions.csv";
        Path path = Paths.get(file);

        CreateFileRequest createFileRequest = new CreateFileRequest(missing,
                path, FilesExtension.CSV, TypeData.MISSING);

        printer.print(createFileRequest);

        Assertions.assertFalse(Files.notExists(path));
        Assertions.assertFalse(Files.isDirectory(path));

        checkFiles(path, "found in file,transaction id,amount,currency code,value date", "SOURCE,125,1.00,USD,2018-03-01");
    }

    private void checkFiles(Path file, String header, String expected) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(file)) {
            String line = reader.readLine();
            Assertions.assertEquals(header, line);
            line = reader.readLine();
            Assertions.assertEquals(expected, line);
        }
    }

    private void fillMismatchAndMissingLists(List<Transaction> mismatch, String idSource, String currency, String dateSource, String idTarget, String dateTarget) {
        Transaction tranMismatchSource = new Transaction(idSource, "", "1.0", currency,
                "", LocalDate.parse(dateSource), "", FilesType.SOURCE);
        Transaction tranMismatchTarget = new Transaction(idTarget, "", "1.0", currency,
                "", LocalDate.parse(dateTarget), "", FilesType.TARGET);

        mismatch.add(tranMismatchSource);
        mismatch.add(tranMismatchTarget);
    }

    private void fillMatchList(List<Transaction> match) {
        Transaction tranMatch = new Transaction("123", "", "1.0", "SAR",
                "", LocalDate.parse("2018-02-01"), "", FilesType.SOURCE);
        match.add(tranMatch);
    }
}
