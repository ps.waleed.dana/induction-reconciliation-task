package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.interfaces.Comparator;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.ReaderResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class DefaultComparatorTest {
    private Comparator comparator;

    @BeforeEach
    public void setup() {
        comparator = new DefaultComparator();
    }

    @Test
    public void canCreate() {
        new DefaultComparator();
    }

    @Test
    public void givenComparative_whenCompareWithRequestSourceNull_thenFail() {
        List<Transaction> target = new LinkedList<>();
        NullPointerException the_request_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> comparator.compare(null, new ReaderResult(target)), "the source request is null");
        Assertions.assertEquals("the source request is null", the_request_is_null.getMessage());
    }

    @Test
    public void givenComparative_whenCompareWithRequestTargetNull_thenFail() {
        List<Transaction> source = new LinkedList<>();
        NullPointerException the_request_is_null = Assertions.assertThrows(NullPointerException.class,
                () -> comparator.compare(new ReaderResult(source), null), "the target request is null");
        Assertions.assertEquals("the target request is null", the_request_is_null.getMessage());
    }

    @Test
    public void givenComparative_whenCompareWithCorrectRequest_thenReturnResultAsExpected() {
        List<Transaction> source = new LinkedList<>();
        List<Transaction> target = new LinkedList<>();
        fillSourceList(source);
        fillTargetList(target);

        ReaderResult sourceResult = new ReaderResult(source);
        ReaderResult targetResult = new ReaderResult(target);

        CompareResult compare = comparator.compare(sourceResult, targetResult);
        Assertions.assertNotNull(compare);

        List<Transaction> match = new LinkedList<>();
        List<Transaction> mismatch = new LinkedList<>();
        List<Transaction> missing = new LinkedList<>();
        fillMatchList(match);
        fillMismatchAndMissingLists(mismatch, "124", "JOD", "2018-01-01", "124", "2018-01-05");
        fillMismatchAndMissingLists(missing, "125", "USD", "2018-03-01", "126", "2018-03-01");

        Assertions.assertEquals(match, compare.getMatch());
        Assertions.assertEquals(mismatch, compare.getMismatch());
        Assertions.assertEquals(missing, compare.getMissing());

    }

    private void fillMismatchAndMissingLists(List<Transaction> mismatch, String idSource, String currency, String dateSource, String idTarget, String dateTarget) {
        Transaction tranMismatchSource = new Transaction(idSource, "", "1.0", currency,
                "", LocalDate.parse(dateSource), "", FilesType.SOURCE);
        Transaction tranMismatchTarget = new Transaction(idTarget, "", "1.0", currency,
                "", LocalDate.parse(dateTarget), "", FilesType.TARGET);

        mismatch.add(tranMismatchSource);
        mismatch.add(tranMismatchTarget);
    }

    private void fillMatchList(List<Transaction> match) {
        Transaction tranMatch = new Transaction("123", "", "1.0", "SAR",
                "", LocalDate.parse("2018-02-01"), "", FilesType.SOURCE);
        match.add(tranMatch);
    }

    private void fillTargetList(List<Transaction> target) {
        Transaction tran1 = new Transaction("123", "", "1.0", "SAR",
                "", LocalDate.parse("2018-02-01"), "", FilesType.TARGET);
        Transaction tran2 = new Transaction("124", "", "1.0", "JOD",
                "", LocalDate.parse("2018-01-05"), "", FilesType.TARGET);
        Transaction tran3 = new Transaction("126", "", "1.0", "USD",
                "", LocalDate.parse("2018-03-01"), "", FilesType.TARGET);
        target.add(tran1);
        target.add(tran2);
        target.add(tran3);
    }

    private void fillSourceList(List<Transaction> source) {
        Transaction tran1 = new Transaction("123", "", "1.0", "SAR",
                "", LocalDate.parse("2018-02-01"), "", FilesType.SOURCE);
        Transaction tran2 = new Transaction("124", "", "1.0", "JOD",
                "", LocalDate.parse("2018-01-01"), "", FilesType.SOURCE);
        Transaction tran3 = new Transaction("125", "", "1.0", "USD",
                "", LocalDate.parse("2018-03-01"), "", FilesType.SOURCE);
        source.add(tran1);
        source.add(tran2);
        source.add(tran3);
    }
}
