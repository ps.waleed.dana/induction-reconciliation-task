package com.progressoft.jip9.compare;

import com.google.gson.Gson;
import com.progressoft.jip9.compare.interfaces.Printer;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.request.CreateFileRequest;
import com.progressoft.jip9.compare.request.PrinterRequest;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.PrinterResult;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class DefaultJSONPrinter implements Printer {

    @Override
    public PrinterResult print(CompareResult result, PrinterRequest printerRequest) {

        List<Transaction> match = result.getMatch();
        List<Transaction> mismatch = result.getMismatch();
        List<Transaction> missing = result.getMissing();

        writeToFile(match, printerRequest.getMatchPath());
        writeToFile(mismatch, printerRequest.getMismatchPath());
        writeToFile(missing, printerRequest.getMissingPath());

        return new PrinterResult(printerRequest.getMatchPath(), printerRequest.getMismatchPath(), printerRequest.getMissingPath());
    }

    @Override
    public void print(CreateFileRequest request) {
        writeToFile(request.getList(), request.getPath());
    }

    private void writeToFile(List<Transaction> list, Path matchPath) {
        Gson gson = new Gson();
        try (BufferedWriter writer = Files.newBufferedWriter(matchPath)) {
            String json = gson.toJson(list);
            writer.write(json);
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }
}
