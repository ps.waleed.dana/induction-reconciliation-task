package com.progressoft.jip9.compare;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.progressoft.jip9.compare.interfaces.Reader;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.request.ReaderRequest;
import com.progressoft.jip9.compare.result.ReaderResult;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class DefaultJSONReader implements Reader {
    @Override
    public ReaderResult read(ReaderRequest request) {
        if (request == null)
            throw new NullPointerException("the request is null");

        ObjectMapper mapper = new ObjectMapper();

        try (FileInputStream src = new FileInputStream(request.getPath().toFile())) {
            Transaction[] list = mapper.readValue(src, Transaction[].class);
            Arrays.stream(list).forEach(transaction -> transaction.setFoundInFile(request.getType()));
            return new ReaderResult(new LinkedList<>(Arrays.asList(list)));
        } catch (JsonMappingException e) {
            throw new IllegalStateException("The JSON file does not correct!", e);
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }
}