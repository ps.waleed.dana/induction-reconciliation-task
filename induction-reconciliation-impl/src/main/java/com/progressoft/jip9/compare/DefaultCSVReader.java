package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.interfaces.Reader;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.request.ReaderRequest;
import com.progressoft.jip9.compare.result.ReaderResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class DefaultCSVReader implements Reader {

    public static final int REFERENCE = 0;
    public static final int DESCRIPTION = 1;
    public static final int AMOUNT = 2;
    public static final int CURRENCY = 3;
    public static final int PURPOSE = 4;
    public static final int DATE = 5;
    public static final int TYPE = 6;

    @Override
    public ReaderResult read(ReaderRequest request) {
        if (request == null)
            throw new NullPointerException("the request is null");
        Path path = request.getPath();

        try (BufferedReader reader = Files.newBufferedReader(path)) {
            List<Transaction> list = new LinkedList<>();

            reader.readLine();
            String line;
            while ((line = reader.readLine()) != null) {
                storeTransaction(list, line, request.getType());
            }
            return new ReaderResult(list);
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    private void storeTransaction(List<Transaction> list, String line, FilesType type) {
        String[] attributes = line.split(",");
        Transaction transaction = new Transaction(attributes[REFERENCE], attributes[DESCRIPTION],
                attributes[AMOUNT], attributes[CURRENCY], attributes[PURPOSE],
                LocalDate.parse(attributes[DATE]), attributes[TYPE], type);
        list.add(transaction);
    }
}
