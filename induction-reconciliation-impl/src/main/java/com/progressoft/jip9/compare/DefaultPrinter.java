package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.interfaces.Printer;
import com.progressoft.jip9.compare.request.CreateFileRequest;
import com.progressoft.jip9.compare.request.PrinterRequest;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.PrinterResult;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DefaultPrinter implements Printer {

    @Override
    public PrinterResult print(CompareResult result, PrinterRequest printerRequest) {
        if (printerRequest == null || result == null)
            throw new NullPointerException("the request is null");
        createDirectoryAndFiles(printerRequest);

        Printer printer = getPrinter(printerRequest.getFileExtension());
        return printer.print(result, printerRequest);
    }

    @Override
    public void print(CreateFileRequest request) {
        if (request == null)
            throw new NullPointerException("the request is null");
        Printer printer = getPrinter(request.getFilesExtension());
        printer.print(request);
    }

    private Printer getPrinter(FilesExtension fileExtension) {
        Printer printer;
        switch (fileExtension) {
            case CSV:
                printer = new DefaultCSVPrinter();
                break;
            case JSON:
                printer = new DefaultJSONPrinter();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + fileExtension);
        }
        return printer;
    }


    private void createDirectoryAndFiles(PrinterRequest printerRequest) {
        try {
            createDirectory(printerRequest);
            createFile(printerRequest.getMatchPath());
            createFile(printerRequest.getMismatchPath());
            createFile(printerRequest.getMissingPath());
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    private void createFile(Path path) throws IOException {
        if (!Files.exists(path))
            Files.createFile(path);
    }

    private void createDirectory(PrinterRequest printerRequest) throws IOException {
        if (!Files.exists(printerRequest.getDirectory()))
            Files.createDirectory(printerRequest.getDirectory());
    }
}