package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.interfaces.Comparator;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.ReaderResult;

import java.util.*;

public class DefaultComparator implements Comparator {

    @Override
    public CompareResult compare(ReaderResult sourceRequest, ReaderResult targetRequest) {
        if (sourceRequest == null)
            throw new NullPointerException("the source request is null");

        if (targetRequest == null)
            throw new NullPointerException("the target request is null");

        CompareRequest compareRequest = new CompareRequest();
        compareRequest.source = sourceRequest.getList();
        compareRequest.target = targetRequest.getList();
        compare(compareRequest);

        return new CompareResult(compareRequest.match, compareRequest.mismatch, compareRequest.missing);
    }

    private void compare(CompareRequest compareRequest) {
        List<Transaction> source = compareRequest.source;
        List<Transaction> target = compareRequest.target;

        List<Transaction> match = compareRequest.match;
        List<Transaction> mismatch = compareRequest.mismatch;
        List<Transaction> missing = compareRequest.missing;

        for (Transaction sourceTransaction : source) {
            if (target.contains(sourceTransaction)) {
                match.add(sourceTransaction);
                target.remove(sourceTransaction);
                continue;
            }
            if (checkIfFound(target, sourceTransaction.getReference())) {
                Transaction targetTransaction = returnTransaction(target, sourceTransaction.getReference());
                mismatch.add(sourceTransaction);
                mismatch.add(targetTransaction);
                target.remove(targetTransaction);
                continue;
            }
            missing.add(sourceTransaction);
        }
        missing.addAll(target);
    }

    private boolean checkIfFound(List<Transaction> goal, String reference) {
        return goal.stream().anyMatch(transaction -> transaction.getReference().equals(reference));
    }

    private Transaction returnTransaction (List<Transaction> goal, String reference){
        return goal.stream().filter(transaction -> transaction.getReference().equals(reference)).findFirst().orElse(null);
    }

    private static class CompareRequest {
        public List<Transaction> match = new LinkedList<>();
        public List<Transaction> mismatch = new LinkedList<>();
        public List<Transaction> missing = new LinkedList<>();
        public List<Transaction> source;
        public List<Transaction> target;
    }
}