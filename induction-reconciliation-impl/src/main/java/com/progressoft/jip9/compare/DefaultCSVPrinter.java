package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.interfaces.Printer;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.request.CreateFileRequest;
import com.progressoft.jip9.compare.request.PrinterRequest;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.PrinterResult;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Currency;
import java.util.List;

public class DefaultCSVPrinter implements Printer {

    @Override
    public PrinterResult print(CompareResult request, PrinterRequest printerRequest) {

        writeToFile(request.getMatch(), printerRequest.getMatchPath(),
                "transaction id,amount,currency code,value date\n", true);
        writeToFile(request.getMismatch(), printerRequest.getMismatchPath(),
                "found in file,transaction id,amount,currency code,value date\n", false);
        writeToFile(request.getMissing(), printerRequest.getMissingPath(),
                "found in file,transaction id,amount,currency code,value date\n", false);

        return new PrinterResult(printerRequest.getMatchPath(), printerRequest.getMismatchPath(), printerRequest.getMissingPath());
    }

    @Override
    public void print(CreateFileRequest request) {
        String header = "";
        boolean isMatch = false;
        switch (request.getTypeData()) {
            case MATCH:
                header = "transaction id,amount,currency code,value date\n";
                isMatch = true;
                break;
            case MISMATCH:
            case MISSING:
                header = "found in file,transaction id,amount,currency code,value date\n";
        }
        writeToFile(request.getList(), request.getPath(), header, isMatch);
    }

    private void writeToFile(List<Transaction> list, Path matchPath, String header, boolean isMatch) {
        try (BufferedWriter writer = Files.newBufferedWriter(matchPath)) {
            writer.write(header);
            for (Transaction current : list) {
                BigDecimal amount = getAmountWithFraction(list, current);
                if (!isMatch) {
                    writer.write(current.getFoundInFile().toString().toUpperCase());
                    writer.write(",");
                }
                writeData(writer, current, amount);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    private void writeData(BufferedWriter writer, Transaction current, BigDecimal amount) throws IOException {
        writer.write(current.getReference());
        writer.write(",");
        writer.write(amount.toString());
        writer.write(",");
        writer.write(current.getCurrencyCode());
        writer.write(",");
        writer.write(current.getDate());
        writer.write("\n");
    }

    private BigDecimal getAmountWithFraction(List<Transaction> list, Transaction s) {
        Currency currency = Currency.getInstance(s.getCurrencyCode());
        int defaultFractionDigits = currency.getDefaultFractionDigits();
        return s.getAmount().setScale(defaultFractionDigits, BigDecimal.ROUND_CEILING);
    }
}
