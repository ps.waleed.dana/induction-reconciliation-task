package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.interfaces.Reader;
import com.progressoft.jip9.compare.request.ReaderRequest;
import com.progressoft.jip9.compare.result.ReaderResult;

public class DefaultReader implements Reader {
    @Override
    public ReaderResult read(ReaderRequest request) {
        if (request == null)
            throw new NullPointerException("the request is null");
        Reader reader;
        switch (request.getExtension()) {
            case CSV:
                reader = new DefaultCSVReader();
                break;
            case JSON:
                reader = new DefaultJSONReader();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + request.getExtension());
        }
        return reader.read(request);
    }
}
