<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="session" tagdir="/WEB-INF/tags" %>


<html>
<head>
    <title>Activity</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
</head>
<body>
<header>

    <nav class="navbar navbar-expand-lg navbar-dark default-color">
        <a class="navbar-brand" href="#"><strong>ProgressSoft</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/reconciliation">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/activity">Activity <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </nav>

</header>

<div class="row" id="mainDiv" style="margin-top: 50px; display: none">
    <div class="col-md-12">
        <table id="main" class="display">
            <thead>
            <tr>
                <th>Session Time</th>
                <th>Reconciliations Activities</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="item" items="${requestScope.sessions}" varStatus="loop">
                <tr>
                    <td>${item}</td>
                    <td>
                        <table class="display" id="${loop.index}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Reconciliation Time</th>
                                <th>Source File Name</th>
                                <th>Target File Name</th>
                                <th>Matched</th>
                                <th>Mismatched</th>
                                <th>Missing</th>
                            </tr>
                            </thead>
                        </table>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>


<script>
    $('#main').DataTable({
        sDom: 'lrtip',
        "paging": false,
        "ordering": false,
        "info": false
    });

    $('#mainDiv').css("display", "block");


    <c:forEach var="item" items="${requestScope.sessions}" varStatus="loop">
    var table = $('#${loop.index}').DataTable({
        'ajax': {
            method: "POST",
            url: '/activity',
            data: function (d) {
                d.session = "${item}";
            },
        },
        sDom: 'lrtip',
        "paging": false,
        "ordering": false,
        "info": false,
        "order": [
            [1, 'asc']
        ],
        'columns': [
            {data: 'id', defaultContent: ''},
            {
                data: "reconciliationTime",
                render: function (data, type, row) {
                    let date = data.date;
                    let time = data.time;
                    return date.year + "-" + date.month + "-" + date.day + " " + time.hour + ":" + time.minute;
                }
            },
            {data: "sourceName"},
            {data: "targetName"},
            {data: "matchCount"},
            {data: "mismatchCount"},
            {data: "missingCount"}
        ]
    });

    table.on('order.dt search.dt', function () {
        table.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    </c:forEach>


</script>
<session:CheckIfEndSession/>
</body>
</html>
