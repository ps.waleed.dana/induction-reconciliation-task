<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="session" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <title>Result</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

    <style>
        body {
            font-family: Arial, serif;
        }

        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
            margin-top: 3%;
        }

        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        .tab button:hover {
            background-color: #ddd;
        }

        .tab button.active {
            background-color: #ccc;
        }

        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>
</head>
<body>
<header>

    <nav class="navbar navbar-expand-lg navbar-dark default-color">
        <a class="navbar-brand" href="#"><strong>ProgressSoft</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/reconciliation">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/result">Result <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/activity">Activity</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </nav>

</header>

<div class="tab">
    <button class="tablinks" id="main" onclick="openTable(event, 'Match')">Match</button>
    <button class="tablinks" onclick="openTable(event, 'Mismatch')">Mismatch</button>
    <button class="tablinks" onclick="openTable(event, 'Missing')">Missing</button>
</div>

<div id="Match" class="tabcontent">
    <div class="row">
        <div class="col-md-12">
            <table id="matchTable" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Transaction ID</th>
                    <th>Amount</th>
                    <th>Currency</th>
                    <th>Value Date</th>

                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Transaction ID</th>
                    <th>Amount</th>
                    <th>Currency</th>
                    <th>Value Date</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <table style="margin-left: 65%; margin-top: 20px">
        <tbody>
        <tr>
            <td>
                <button type="button" class="btn btn-light new">Compare new files</button>
            </td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Download
                    </button>

                    <div class="dropdown-menu dropdown-primary">
                        <a class="dropdown-item" href="/resultApi?file=match.json">JSON</a>
                        <a class="dropdown-item" href="/resultApi?file=match.csv">CSV</a>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<div id="Mismatch" class="tabcontent">
    <div class="row">
        <div class="col-md-12">
            <table id="mismatchTable" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Transaction ID</th>
                    <th>Found in file</th>
                    <th>Amount</th>
                    <th>Currency</th>
                    <th>Value Date</th>

                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Transaction ID</th>
                    <th>Found in file</th>
                    <th>Amount</th>
                    <th>Currency</th>
                    <th>Value Date</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <table style="margin-left: 65%; margin-top: 20px">
        <tbody>
        <tr>
            <td>
                <button type="button" class="btn btn-light new">Compare new files</button>
            </td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Download
                    </button>

                    <div class="dropdown-menu dropdown-primary">
                        <a class="dropdown-item" href="/resultApi?file=mismatch.json">JSON</a>
                        <a class="dropdown-item" href="/resultApi?file=mismatch.csv">CSV</a>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

</div>

<div id="Missing" class="tabcontent">
    <div class="row">
        <div class="col-md-12">
            <table id="missingTable" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Transaction ID</th>
                    <th>Found in file</th>
                    <th>Amount</th>
                    <th>Currency</th>
                    <th>Value Date</th>

                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Transaction ID</th>
                    <th>Found in file</th>
                    <th>Amount</th>
                    <th>Currency</th>
                    <th>Value Date</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <table style="margin-left: 65%; margin-top: 20px">
        <tbody>
        <tr>
            <td>
                <button type="button" class="btn btn-light new">Compare new files</button>
            </td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Download
                    </button>

                    <div class="dropdown-menu dropdown-primary">
                        <a class="dropdown-item" href="/resultApi?file=missing.json">JSON</a>
                        <a class="dropdown-item" href="/resultApi?file=missing.csv">CSV</a>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

<script>

    $(".new").click(function () {
        window.location = "/reconciliation";
    });

    var match = $('#matchTable').DataTable({
        'ajax': {
            method: "POST",
            url: '/resultApi',
            data: function (d) {
                d.type = "match";
            },
        },
        "order": [
            [1, 'asc']
        ],
        'columns': [
            {data: 'id', defaultContent: ''},
            {data: "reference"},
            {data: "amount"},
            {data: "currencyCode"},
            {
                data: "date",
                render: function (data, type, row) {
                    return data.year + "-" + data.month + "-" + data.day;
                }
            }
        ]
    });

    match.on('order.dt search.dt', function () {
        match.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    var mismatch = $('#mismatchTable').DataTable({
        'ajax': {
            method: "POST",
            url: '/resultApi',
            data: function (d) {
                d.type = "mismatch";
            },
        },
        "order": [
            [1, 'asc']
        ],
        'columns': [
            {data: 'id', defaultContent: ''},
            {data: "reference"},
            {data: "foundInFile"},
            {data: "amount"},
            {data: "currencyCode"},
            {
                data: "date",
                render: function (data, type, row) {
                    return data.year + "-" + data.month + "-" + data.day;
                }
            }
        ]
    });

    mismatch.on('order.dt search.dt', function () {
        mismatch.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();


    var missing = $('#missingTable').DataTable({
        'ajax': {
            method: "POST",
            url: '/resultApi',
            data: function (d) {
                d.type = "missing";
            },
        },
        "order": [
            [1, 'asc']
        ],
        'columns': [
            {data: 'id', defaultContent: ''},
            {data: "reference"},
            {data: "foundInFile"},
            {data: "amount"},
            {data: "currencyCode"},
            {
                data: "date",
                render: function (data, type, row) {
                    return data.year + "-" + data.month + "-" + data.day;
                }
            }
        ]
    });

    missing.on('order.dt search.dt', function () {
        missing.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    openTable(event, 'Match');

    function openTable(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

</script>
<session:CheckIfEndSession/>
</body>
</html>
