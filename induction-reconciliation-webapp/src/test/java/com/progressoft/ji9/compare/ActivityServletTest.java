package com.progressoft.ji9.compare;

import com.google.gson.Gson;
import com.progressoft.jip9.compare.implement.DBReconciliationsProcessRepository;
import com.progressoft.jip9.compare.implement.DBUsersProcessRepository;
import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.ReconciliationsProcess;
import com.progressoft.jip9.compare.interfaces.UsersProcess;
import com.progressoft.jip9.compare.modul.Reconciliation;
import com.progressoft.jip9.compare.modul.User;
import com.progressoft.jip9.compare.servlets.ActivityServlet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityServletTest extends Mockito{

    private ReconciliationsProcess reconciliationsProcess;
    private UsersProcess usersProcess;
    private Gson gson;

    @BeforeEach
    public void setup() {
        gson = new Gson();
        DataSource dataSource = DatabaseInitializer.getH2DataSource();
        DatabaseInitializer initializer = new DatabaseInitializer(dataSource);
        reconciliationsProcess = new DBReconciliationsProcessRepository(initializer, dataSource);
        usersProcess = new DBUsersProcessRepository(initializer, dataSource);
        reconciliationsProcess.deleteAllReconciliations();
        usersProcess.deleteAllUsers();
    }

    @Test
    public void givenActivityServlet_whenDoPost_thenReturnReconciliations() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        PrintWriter writer = mock(PrintWriter.class);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        usersProcess.addUser(new User("admin", "admin"));

        Map<String, Object> finalResult = new HashMap<>();
        LocalDateTime now = LocalDateTime.now();
        Reconciliation reconciliation = new Reconciliation(1, now, now, "waleed"
                , "mohammad", 3, 4, 3);

        reconciliationsProcess.addReconciliation(reconciliation);
        List<Reconciliation> reconciliations = reconciliationsProcess.getReconciliations(1, now.format(dateTimeFormatter));
        finalResult.put("data" ,reconciliations);
        String json = gson.toJson(finalResult);

        when(response.getWriter()).thenReturn(writer);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(1);
        when(request.getParameter("session")).thenReturn(now.format(dateTimeFormatter));

        ActivityServlet activityServlet = new ActivityServlet(reconciliationsProcess);
        activityServlet.init();
        activityServlet.doPost(request, response);

        verify(response).setContentType("application/json");
        verify(writer).write(json);
        verify(writer).flush();
    }

    @Test
    public void givenActivityServlet_whenDoGet_thenSessionsTimeWillStoreInSession() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(1);
        when(request.getRequestDispatcher("/WEB-INF/views/activity.jsp")).thenReturn(dispatcher);

        ActivityServlet activityServlet = new ActivityServlet(reconciliationsProcess);
        activityServlet.doGet(request, response);

        List<String> sessionsTime = reconciliationsProcess.getSessionsTime(1);
        verify(request).setAttribute("sessions", sessionsTime);
        verify(dispatcher).forward(request, response);
    }
}
