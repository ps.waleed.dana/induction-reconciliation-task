package com.progressoft.ji9.compare;

import com.progressoft.jip9.compare.implement.DBReconciliationsProcessRepository;
import com.progressoft.jip9.compare.implement.DBUsersProcessRepository;
import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.ReconciliationsProcess;
import com.progressoft.jip9.compare.interfaces.UsersProcess;
import com.progressoft.jip9.compare.modul.User;
import com.progressoft.jip9.compare.servlets.LoginServlet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;

public class LoginServletTest extends Mockito {

    private DataSource dataSource;
    private DatabaseInitializer initializer;
    private UsersProcess usersProcess;
    private ReconciliationsProcess reconciliationsProcess;

    @BeforeEach
    public void canCreate() {
        dataSource = DatabaseInitializer.getH2DataSource();
        initializer = new DatabaseInitializer(dataSource);
        usersProcess = new DBUsersProcessRepository(initializer, dataSource);
        reconciliationsProcess = new DBReconciliationsProcessRepository(initializer, dataSource);
        reconciliationsProcess.deleteAllReconciliations();
        usersProcess.deleteAllUsers();
    }

    @Test
    public void givenWrongDataUser_whenLogin_thenReturnAttributeWrong() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getParameter("username")).thenReturn("admin");
        when(request.getParameter("password")).thenReturn("admn");
        when(request.getRequestDispatcher("/WEB-INF/views/login.jsp")).thenReturn(dispatcher);
        usersProcess.addUser(new User("admin", "admin"));

        LoginServlet loginServlet = new LoginServlet(usersProcess);
        loginServlet.doPost(request, response);

        verify(request).setAttribute("wrong", "your username or password are incorrect");
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void givenRightDataUser_whenLogin_thenRedirectToNextPage() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getParameter("username")).thenReturn("admin");
        when(request.getParameter("password")).thenReturn("admin");
        when(request.getSession()).thenReturn(session);
        usersProcess.addUser(new User("admin", "admin"));

        LoginServlet loginServlet = new LoginServlet(usersProcess);
        loginServlet.doPost(request, response);

        verify(session).setAttribute("user", 1);
        verify(response).sendRedirect(request.getContextPath() + "/reconciliation");
    }
}
