package com.progressoft.ji9.compare;

import com.google.gson.Gson;
import com.progressoft.jip9.compare.API.ResultApiServlet;
import com.progressoft.jip9.compare.DefaultPrinter;
import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.interfaces.Printer;
import com.progressoft.jip9.compare.modul.Transaction;
import com.progressoft.jip9.compare.result.CompareResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ResultApiServletTest extends Mockito {

    private Gson gson;
    private Printer printer;

    @BeforeEach
    public void setup() {
        gson = new Gson();
        printer = new DefaultPrinter();
    }

    @Test
    public void givenResultApiServlet_whenDoPost_thenSuccess() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        PrintWriter writer = mock(PrintWriter.class);

        List<Transaction> match = new LinkedList<>();
        List<Transaction> mismatch = new LinkedList<>();
        List<Transaction> missing = new LinkedList<>();
        Map<String, Object> finalResult = new HashMap<>();

        Transaction transaction = new Transaction("", "", "140", "JOD", "",
                LocalDate.parse("2020-01-20"), "", FilesType.SOURCE);
        match.add(transaction);

        CompareResult compareResult = new CompareResult(match, mismatch, missing);
        finalResult.put("data", compareResult.getMatch());

        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("result")).thenReturn(compareResult);
        when(request.getParameter("type")).thenReturn("match");
        when(response.getWriter()).thenReturn(writer);

        String json = gson.toJson(finalResult);

        ResultApiServlet servlet = new ResultApiServlet(printer);
        servlet.init();
        servlet.doPost(request, response);

        verify(response).setContentType("application/json");
        verify(writer).write(json);
        verify(writer).flush();
    }

    @Test
    public void givenResultApiServlet_whenDoGET_thenSuccess() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        PrintWriter writer = mock(PrintWriter.class);

        List<Transaction> match = new LinkedList<>();
        List<Transaction> mismatch = new LinkedList<>();
        List<Transaction> missing = new LinkedList<>();

        Transaction transaction = new Transaction("", "", "140", "JOD", "",
                LocalDate.parse("2020-01-20"), "", FilesType.SOURCE);
        match.add(transaction);

        CompareResult compareResult = new CompareResult(match, mismatch, missing);

        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("result")).thenReturn(compareResult);
        when(request.getParameter("file")).thenReturn("match.json");
        when(response.getWriter()).thenReturn(writer);

        ResultApiServlet servlet = new ResultApiServlet(printer);
        servlet.init();
        servlet.doGet(request, response);

        String json = gson.toJson(compareResult.getMatch());
        verify(response).setContentType("application/octet-stream");
        verify(response).setHeader("Content-Disposition", "attachment;fileName=\"match.json\"");
        verify(writer).print(json);
        verify(writer).flush();
    }
}