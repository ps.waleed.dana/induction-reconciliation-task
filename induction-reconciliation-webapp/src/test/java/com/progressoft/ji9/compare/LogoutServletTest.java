package com.progressoft.ji9.compare;

import com.progressoft.jip9.compare.servlets.LogoutServlet;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutServletTest extends Mockito{

    @Test
    public void givenLogoutServlet_whenDoGet_thenSendRedirectToLogin() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);

        LogoutServlet logoutServlet = new LogoutServlet();
        logoutServlet.doGet(request, response);

        verify(session).invalidate();
        verify(response).sendRedirect("/login");
    }
}
