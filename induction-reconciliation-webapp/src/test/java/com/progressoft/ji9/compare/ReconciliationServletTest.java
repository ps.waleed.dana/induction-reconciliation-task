package com.progressoft.ji9.compare;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.progressoft.jip9.compare.DefaultComparator;
import com.progressoft.jip9.compare.DefaultReader;
import com.progressoft.jip9.compare.implement.DBReconciliationsProcessRepository;
import com.progressoft.jip9.compare.implement.DBUsersProcessRepository;
import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.Comparator;
import com.progressoft.jip9.compare.interfaces.Reader;
import com.progressoft.jip9.compare.interfaces.ReconciliationsProcess;
import com.progressoft.jip9.compare.interfaces.UsersProcess;
import com.progressoft.jip9.compare.modul.User;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.servlets.ReconciliationServlet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class ReconciliationServletTest extends Mockito {

    private Gson gson;
    private ReconciliationsProcess reconciliationsProcess;
    private UsersProcess usersProcess;
    private Reader reader;
    private Comparator comparator;


    @BeforeEach
    public void setup() {
        gson = new Gson();
        DataSource dataSource = DatabaseInitializer.getH2DataSource();
        DatabaseInitializer initializer = new DatabaseInitializer(dataSource);
        reconciliationsProcess = new DBReconciliationsProcessRepository(initializer, dataSource);
        usersProcess = new DBUsersProcessRepository(initializer, dataSource);
        reader = new DefaultReader();
        comparator = new DefaultComparator();
        reconciliationsProcess.deleteAllReconciliations();
        usersProcess.deleteAllUsers();
    }

    @Test
    public void givenReconciliationServlet_whenDoPost_thenWillSaveCompareResultInSession() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        Part partSource = mock(Part.class);
        Part partTarget = mock(Part.class);
        Path sourceFile = Paths.get("..", "request", "bank-transactions.csv");
        Path targetFile = Paths.get("..", "request", "online-banking-transactions.json");
        InputStream sourceStream = new FileInputStream(sourceFile.toFile());
        InputStream targetStream = new FileInputStream(targetFile.toFile());
        Reader reader = new DefaultReader();
        Comparator comparator = new DefaultComparator();

        when(request.getSession(true)).thenReturn(session);
        when(request.getSession(true).getAttribute("user")).thenReturn(1);
        when(request.getSession(true).getAttribute("timeSession")).thenReturn(LocalDateTime.now());
        when(request.getPart("sourceFile")).thenReturn(partSource);
        when(request.getPart("targetFile")).thenReturn(partTarget);
        when(request.getParameter("sourceName")).thenReturn("waleed");
        when(request.getParameter("targetName")).thenReturn("mohammad");
        when(request.getParameter("sourceType")).thenReturn("CSV");
        when(request.getParameter("targetType")).thenReturn("JSON");
        when(partSource.getInputStream()).thenReturn(sourceStream);
        when(partTarget.getInputStream()).thenReturn(targetStream);

        usersProcess.addUser(new User("admin", "admin"));
        ReconciliationServlet reconciliationServlet = new ReconciliationServlet(reader, comparator, reconciliationsProcess);
        reconciliationServlet.doPost(request, response);

        String json = "{\"match\":[{\"reference\":\"TR-47884222201\",\"amount\":140,\"currencyCode\":\"USD\",\"date\":{\"year\":2020,\"month\":1,\"day\":20},\"transDescription\":\"online transfer\",\"purpose\":\"donation\",\"transType\":\"D\",\"foundInFile\":\"SOURCE\"},{\"reference\":\"TR-47884222203\",\"amount\":5000,\"currencyCode\":\"JOD\",\"date\":{\"year\":2020,\"month\":1,\"day\":25},\"transDescription\":\"counter withdrawal\",\"purpose\":\"\",\"transType\":\"D\",\"foundInFile\":\"SOURCE\"},{\"reference\":\"TR-47884222206\",\"amount\":500.0,\"currencyCode\":\"USD\",\"date\":{\"year\":2020,\"month\":2,\"day\":10},\"transDescription\":\"atm withdrwal\",\"purpose\":\"\",\"transType\":\"D\",\"foundInFile\":\"SOURCE\"}],\"mismatch\":[{\"reference\":\"TR-47884222202\",\"amount\":20.0000,\"currencyCode\":\"JOD\",\"date\":{\"year\":2020,\"month\":1,\"day\":22},\"transDescription\":\"atm withdrwal\",\"purpose\":\"\",\"transType\":\"D\",\"foundInFile\":\"SOURCE\"},{\"reference\":\"TR-47884222202\",\"amount\":30.000,\"currencyCode\":\"JOD\",\"date\":{\"year\":2020,\"month\":1,\"day\":22},\"purpose\":\"donation\",\"foundInFile\":\"TARGET\"},{\"reference\":\"TR-47884222205\",\"amount\":60.0,\"currencyCode\":\"JOD\",\"date\":{\"year\":2020,\"month\":2,\"day\":2},\"transDescription\":\"atm withdrwal\",\"purpose\":\"\",\"transType\":\"D\",\"foundInFile\":\"SOURCE\"},{\"reference\":\"TR-47884222205\",\"amount\":60.000,\"currencyCode\":\"JOD\",\"date\":{\"year\":2020,\"month\":2,\"day\":3},\"purpose\":\"\",\"foundInFile\":\"TARGET\"}],\"missing\":[{\"reference\":\"TR-47884222204\",\"amount\":1200.000,\"currencyCode\":\"JOD\",\"date\":{\"year\":2020,\"month\":1,\"day\":31},\"transDescription\":\"salary\",\"purpose\":\"donation\",\"transType\":\"C\",\"foundInFile\":\"SOURCE\"},{\"reference\":\"TR-47884222217\",\"amount\":12000.000,\"currencyCode\":\"JOD\",\"date\":{\"year\":2020,\"month\":2,\"day\":14},\"purpose\":\"salary\",\"foundInFile\":\"TARGET\"},{\"reference\":\"TR-47884222245\",\"amount\":420.00,\"currencyCode\":\"USD\",\"date\":{\"year\":2020,\"month\":1,\"day\":12},\"purpose\":\"loan\",\"foundInFile\":\"TARGET\"}]}";
        CompareResult compareResult = gson.fromJson(json, new TypeToken<CompareResult>() {
        }.getType());
        verify(session).setAttribute("result", compareResult);
        verify(response).sendRedirect(request.getContextPath() + "/result");
    }

    @Test
    public void givenReconciliationServlet_whenDoGet_thenRedirectToReconciliationPage() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getRequestDispatcher("/WEB-INF/views/reconciliation.jsp")).thenReturn(dispatcher);

        ReconciliationServlet reconciliationServlet = new ReconciliationServlet(reader, comparator, reconciliationsProcess);
        reconciliationServlet.doGet(request, response);

        verify(dispatcher).forward(request, response);
    }
}