package com.progressoft.ji9.compare;

import com.progressoft.jip9.compare.filters.AuthenticationFilter;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilterTest extends Mockito {

    @Test
    public void givenAuthenticationFilter_whenSessionNotNull_thenThenContinueRequest() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(1);

        AuthenticationFilter authenticationFilter = new AuthenticationFilter();
        authenticationFilter.doFilter(request, response, filterChain);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void givenAuthenticationFilter_whenSessionNull_thenThenRedirectToLoginPage() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getSession(false)).thenReturn(null);
        AuthenticationFilter authenticationFilter = new AuthenticationFilter();
        authenticationFilter.doFilter(request, response, filterChain);
        verify(response).sendRedirect("/login");
    }
}
