package com.progressoft.ji9.compare;

import com.progressoft.jip9.compare.servlets.ResultServlet;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResultServletTest extends Mockito {

    @Test
    public void givenResultServlet_whenDoGet_thenRedirectToResultPage() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getRequestDispatcher("/WEB-INF/views/result.jsp")).thenReturn(dispatcher);

        ResultServlet resultServlet = new ResultServlet();
        resultServlet.doGet(request, response);

        verify(dispatcher).forward(request, response);
    }
}
