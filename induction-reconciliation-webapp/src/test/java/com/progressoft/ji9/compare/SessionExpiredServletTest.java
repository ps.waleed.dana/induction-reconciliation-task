package com.progressoft.ji9.compare;

import com.progressoft.jip9.compare.servlets.SessionExpiredServlet;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class SessionExpiredServletTest extends Mockito {

    @Test
    public void givenSessionExpiredServlet_whenSetSessionNotNull_thenPrintResponseFalse() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        PrintWriter writer = mock(PrintWriter.class);

        when(request.getSession(false)).thenReturn(session);
        when(response.getWriter()).thenReturn(writer);

        SessionExpiredServlet sessionExpiredServlet = new SessionExpiredServlet();
        sessionExpiredServlet.doGet(request, response);

        verify(writer).print("false");
        verify(writer).flush();
    }

    @Test
    public void givenSessionExpiredServlet_whenSetSessionNull_thenPrintResponseTrue() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        PrintWriter writer = mock(PrintWriter.class);

        when(request.getSession(false)).thenReturn(null);
        when(response.getWriter()).thenReturn(writer);

        SessionExpiredServlet sessionExpiredServlet = new SessionExpiredServlet();
        sessionExpiredServlet.doGet(request, response);

        verify(writer).print("true");
        verify(writer).flush();
    }
}
