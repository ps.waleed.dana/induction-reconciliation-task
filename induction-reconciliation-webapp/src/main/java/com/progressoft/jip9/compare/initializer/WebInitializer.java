package com.progressoft.jip9.compare.initializer;

import com.progressoft.jip9.compare.API.ResultApiServlet;
import com.progressoft.jip9.compare.*;
import com.progressoft.jip9.compare.filters.AuthenticationFilter;
import com.progressoft.jip9.compare.filters.LoginFilter;
import com.progressoft.jip9.compare.implement.DBReconciliationsProcessRepository;
import com.progressoft.jip9.compare.implement.DBUsersProcessRepository;
import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.*;
import com.progressoft.jip9.compare.modul.User;
import com.progressoft.jip9.compare.servlets.*;

import javax.servlet.*;
import javax.sql.DataSource;
import java.util.EnumSet;
import java.util.Set;

public class WebInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> set, ServletContext ctx) throws ServletException {
        DataSource dataSource = DatabaseInitializer.getMysqlDataSource();
        DatabaseInitializer initializer = new DatabaseInitializer(dataSource);
        UsersProcess usersProcess = new DBUsersProcessRepository(initializer, dataSource);
        if (!usersProcess.isUsernameExists("admin"))
            usersProcess.addUser(new User("admin", "admin"));

        // TODO why this is called 4 times
        initializer.createTableUsersIfNotExists();
        initializer.createTableUsersIfNotExists();
        initializer.createTableUsersIfNotExists();
        initializer.createTableUsersIfNotExists();

        ReconciliationsProcess reconciliationsProcess = new DBReconciliationsProcessRepository(initializer, dataSource);

        registerReconciliationServlet(ctx, reconciliationsProcess);
        registerActivityServlet(ctx, reconciliationsProcess);
        registerResultServlet(ctx);
        registerLoginServlet(ctx, usersProcess);
        registerLogoutServlet(ctx);
        registerResultApiServlet(ctx);
        registerSessionExpiredServlet(ctx);
        registerAuthenticationFilter(ctx);
        registerLoginFilter(ctx);
    }

    private void registerSessionExpiredServlet(ServletContext ctx) {
        SessionExpiredServlet sessionExpiredServlet = new SessionExpiredServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("sessionExpiredServlet", sessionExpiredServlet);
        registration.addMapping("/session");
    }

    private void registerLoginFilter(ServletContext ctx) {
        FilterRegistration.Dynamic loginFilter = ctx.addFilter("loginFilter", new LoginFilter());
        loginFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD), true, "/login");
    }

    private void registerResultApiServlet(ServletContext ctx) {
        Printer printer = new DefaultPrinter();
        ResultApiServlet resultApiServlet = new ResultApiServlet(printer);
        ServletRegistration.Dynamic registration = ctx.addServlet("resultApiServlet", resultApiServlet);
        registration.addMapping("/resultApi");
    }

    private void registerAuthenticationFilter(ServletContext ctx) {
        FilterRegistration.Dynamic authenticationFilter = ctx.addFilter("authenticationFilter", new AuthenticationFilter());
        authenticationFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD), true,
                "/activity", "/reconciliation", "/result", "/resultApi");
    }

    private void registerLogoutServlet(ServletContext ctx) {
        LogoutServlet logoutServlet = new LogoutServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("logoutServlet", logoutServlet);
        registration.addMapping("/logout");
    }

    private void registerLoginServlet(ServletContext ctx, UsersProcess usersProcess) {
        LoginServlet loginServlet = new LoginServlet(usersProcess);
        ServletRegistration.Dynamic registration = ctx.addServlet("loginServlet", loginServlet);
        registration.addMapping("/login");
    }

    private void registerResultServlet(ServletContext ctx) {
        ResultServlet resultServlet = new ResultServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("compareServlet", resultServlet);
        registration.addMapping("/result");

    }

    private void registerActivityServlet(ServletContext ctx, ReconciliationsProcess reconciliationsProcess) {
        ActivityServlet activityServlet = new ActivityServlet(reconciliationsProcess);
        ServletRegistration.Dynamic registration = ctx.addServlet("activityServlet", activityServlet);
        registration.addMapping("/activity");
    }

    private void registerReconciliationServlet(ServletContext ctx, ReconciliationsProcess reconciliationsProcess) {
        Reader reader = new DefaultReader();
        Comparator comparator = new DefaultComparator();
        ReconciliationServlet reconciliationServlet = new ReconciliationServlet(reader, comparator, reconciliationsProcess);
        ServletRegistration.Dynamic registration = ctx.addServlet("reconciliationServlet", reconciliationServlet);
        registration.addMapping("/reconciliation");
        MultipartConfigElement multipartConfig = new MultipartConfigElement(
                null,
                1024 * 1024 * 2,
                1024 * 1024 * 2,
                1024 * 512
        );
        registration.setMultipartConfig(multipartConfig);
    }
}
