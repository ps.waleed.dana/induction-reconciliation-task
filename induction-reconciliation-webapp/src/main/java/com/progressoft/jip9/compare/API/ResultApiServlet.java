package com.progressoft.jip9.compare.API;
// TODO small package names
import com.google.gson.Gson;
import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.TypeData;
import com.progressoft.jip9.compare.interfaces.Printer;
import com.progressoft.jip9.compare.request.CreateFileRequest;
import com.progressoft.jip9.compare.result.CompareResult;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class ResultApiServlet extends HttpServlet {
    private Gson gson;
    private final Printer printer;

    public ResultApiServlet(Printer printer) {
        this.printer = printer;
    }

    @Override
    public void init() throws ServletException {
        gson = new Gson();
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CompareResult result = getCompareResult(req);
        String type = req.getParameter("type");
        Map<String, Object> finalResult = new HashMap<>();
        resp.setContentType("application/json");

        fillFinalResult(result, type, finalResult);

        PrintWriter writer = resp.getWriter();
        String json = gson.toJson(finalResult);
        writer.write(json);
        writer.flush();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CompareResult result = getCompareResult(req);
        String file = req.getParameter("file");
        String[] split = file.split("\\.");
        String type = split[0];
        String extension = split[1];

        resp.setContentType("application/octet-stream");
        resp.setHeader("Content-Disposition", "attachment;fileName=\"" + file + "\"");

        Path path = Files.createTempFile(type, extension);
        CreateFileRequest createFileRequest = getCreateFileRequest(result, type, extension, path);
        printer.print(createFileRequest);
        printResponse(resp, path, FilesExtension.parse(extension));
    }

    private CreateFileRequest getCreateFileRequest(CompareResult result, String type, String extension, Path path) {
        switch (type) {
            case "match":
                assert result != null;
                return new CreateFileRequest(result.getMatch(), path, FilesExtension.parse(extension), TypeData.parse(type));
            case "mismatch":
                assert result != null;
                return new CreateFileRequest(result.getMismatch(), path, FilesExtension.parse(extension), TypeData.parse(type));
            case "missing":
                assert result != null;
                return new CreateFileRequest(result.getMissing(), path, FilesExtension.parse(extension), TypeData.parse(type));
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }
    }

    private void printResponse(HttpServletResponse resp, Path path, FilesExtension extension) throws IOException {
        PrintWriter writer = resp.getWriter();
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (extension.equals(FilesExtension.CSV))
                    writer.println(line);
                else
                    writer.print(line);
            }
            writer.flush();
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    private CompareResult getCompareResult(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("result") != null)
            return (CompareResult) session.getAttribute("result");
        return null;
    }

    private void fillFinalResult(CompareResult result, String type, Map<String, Object> finalResult) {
        switch (type){
            case "match":
                assert result != null;
                finalResult.put("data", result.getMatch());
                break;
            case "mismatch":
                assert result != null;
                finalResult.put("data", result.getMismatch());
                break;
            case "missing":
                assert result != null;
                finalResult.put("data", result.getMissing());

        }
    }
}
