package com.progressoft.jip9.compare.servlets;

import com.progressoft.jip9.compare.interfaces.UsersProcess;
import com.progressoft.jip9.compare.modul.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;

public class LoginServlet extends HttpServlet {
    private final UsersProcess usersProcess;

    public LoginServlet(UsersProcess usersProcess) {
        this.usersProcess = usersProcess;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        int id = usersProcess.login(new User(username, password));

        if (setWrongAttributeIfWrongLoginData(req, resp, id))
            return;

        HttpSession session = req.getSession();
        session.setAttribute("user", id);
        session.setAttribute("timeSession", LocalDateTime.now());

        resp.sendRedirect(req.getContextPath() + "/reconciliation");

    }

    private boolean setWrongAttributeIfWrongLoginData(HttpServletRequest req, HttpServletResponse resp, int id) throws ServletException, IOException {
        if (id == 0){
            req.setAttribute("wrong", "your username or password are incorrect");
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
            requestDispatcher.forward(req, resp);
            return true;
        }
        return false;
    }
}
