package com.progressoft.jip9.compare.servlets;

import com.progressoft.jip9.compare.interfaces.Comparator;
import com.progressoft.jip9.compare.interfaces.Reader;
import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.interfaces.ReconciliationsProcess;
import com.progressoft.jip9.compare.modul.Reconciliation;
import com.progressoft.jip9.compare.request.ReaderRequest;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.ReaderResult;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;

public class ReconciliationServlet extends HttpServlet {

    private final Reader reader;
    private final Comparator comparator;
    private final ReconciliationsProcess reconciliationsProcess;

    public ReconciliationServlet(Reader reader, Comparator comparator, ReconciliationsProcess reconciliationsProcess) {
        this.reader = reader;
        this.comparator = comparator;
        this.reconciliationsProcess = reconciliationsProcess;
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/reconciliation.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part source = req.getPart("sourceFile");
        Part target = req.getPart("targetFile");
        String sourceName = req.getParameter("sourceName");
        String targetName = req.getParameter("targetName");
        String sourceType = req.getParameter("sourceType");
        String targetType = req.getParameter("targetType");

        Path sourceFile = Files.createTempFile(sourceName, sourceType.toLowerCase());
        Path targetFile = Files.createTempFile(targetName, targetType.toLowerCase());

        writeToFile(sourceFile, getFileContent(source));
        writeToFile(targetFile, getFileContent(target));

        ReaderResult readerSourceResult = getReaderResult(sourceType, sourceFile, FilesType.SOURCE);
        ReaderResult readerTargetResult = getReaderResult(targetType, targetFile, FilesType.TARGET);

        CompareResult compare = comparator.compare(readerSourceResult, readerTargetResult);

        storeTransaction(req, compare, sourceName, targetName);
        resp.sendRedirect(req.getContextPath() + "/result");
    }

    private void storeTransaction(HttpServletRequest req, CompareResult compare, String sourceName, String targetName) {
        HttpSession session = req.getSession(true);
        session.setAttribute("result", compare);
        Reconciliation reconciliations = getReconciliation(compare, sourceName, targetName, session);
        reconciliationsProcess.addReconciliation(reconciliations);
    }

    private Reconciliation getReconciliation(CompareResult compare, String sourceName, String targetName, HttpSession session) {
        LocalDateTime timeSession = (LocalDateTime) session.getAttribute("timeSession");
        LocalDateTime reconciliationTime = LocalDateTime.now();
        int user = (int) session.getAttribute("user");
        int matchSize = compare.getMatchSize();
        int mismatchSize = compare.getMismatchSize();
        int missingSize = compare.getMissingSize();

        Reconciliation reconciliations = new Reconciliation(user, timeSession, reconciliationTime,
                sourceName, targetName, matchSize, mismatchSize, missingSize);
        return reconciliations;
    }

    private ReaderResult getReaderResult(String extension, Path file, FilesType filesType) {
        ReaderRequest readSource = new ReaderRequest(file, FilesExtension.parse(extension.toUpperCase()), filesType);
        return reader.read(readSource);
    }

    private void writeToFile(Path path, String content) throws IOException {
        try (OutputStream fos = new FileOutputStream(path.toFile());
             OutputStreamWriter writer = new OutputStreamWriter(fos)) {
            writer.write(content);
        }
    }

    private String getFileContent(Part part) throws IOException {
        StringBuilder data = new StringBuilder();
        try (InputStream inputStream = part.getInputStream()) {
            int c;
            while ((c = inputStream.read()) != -1) {
                data.append((char) c);
            }
        }
        return data.toString();
    }
}
