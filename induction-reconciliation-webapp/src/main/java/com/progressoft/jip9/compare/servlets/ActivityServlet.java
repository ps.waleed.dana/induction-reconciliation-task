package com.progressoft.jip9.compare.servlets;

import com.google.gson.Gson;
import com.progressoft.jip9.compare.interfaces.ReconciliationsProcess;
import com.progressoft.jip9.compare.modul.Reconciliation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityServlet  extends HttpServlet {
    private final ReconciliationsProcess reconciliationsProcess;
    private Gson gson;

    public ActivityServlet(ReconciliationsProcess reconciliationsProcess) {
        this.reconciliationsProcess = reconciliationsProcess;
    }

    @Override
    public void init() throws ServletException {
        gson = new Gson();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        int user = (int) session.getAttribute("user");

        List<String> sessionsTime = reconciliationsProcess.getSessionsTime(user);
        req.setAttribute("sessions", sessionsTime);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/activity.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        int user = (int) session.getAttribute("user");
        String sessionTime = req.getParameter("session");

        List<Reconciliation> reconciliations = reconciliationsProcess.getReconciliations(user, sessionTime);
        resp.setContentType("application/json");

        Map<String, Object> finalResult = new HashMap<>();
        finalResult.put("data", reconciliations);
        String json = gson.toJson(finalResult);

        PrintWriter writer = resp.getWriter();
        writer.write(json);
        writer.flush();
    }
}
