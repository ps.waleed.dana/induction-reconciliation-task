<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>

    $(function () {
        // TODO mostly this would keep the session alive
        setInterval(oneSecondFunction, 5000);
    });

    function oneSecondFunction() {
        $.ajax({
            method: "GET",
            url: "/session",
            complete: function (data) {
                if (data.responseText === "true")
                    window.location = "/login";
            }
        });
    }

</script>