<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Login</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/style/style.css">
</head>
<body style="background-color: #ebebeb">

<div class="container-fluid">
    <div class="row" style="margin-top: 10%">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <form method="post" class="text-center border border-light p-5" action="/login" style="background-color: white">

                        <p class="h4 mb-4">Sign in</p>

                        <input type="text" name="username" id="username" class="form-control mb-4" placeholder="Username">

                        <input type="password" id="password" name="password" class="form-control mb-4" placeholder="Password">
                        <c:if test="${not empty requestScope.wrong}">
                              <span class="text-danger">${requestScope.wrong}</span><br>
                        </c:if>
                        <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

                    </form>
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>

</body>
</html>
