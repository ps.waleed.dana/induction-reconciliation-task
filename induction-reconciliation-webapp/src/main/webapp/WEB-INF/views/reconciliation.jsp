<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="session" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <title>
        Compare
    </title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/style/style.css">
</head>
<body>
<header>

    <nav class="navbar navbar-expand-lg navbar-dark default-color">
        <a class="navbar-brand" href="#"><strong>ProgressSoft</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/reconciliation">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/activity">Activity</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </nav>

</header>


<div class="content" style="margin-top: 3%">
    <div class="content__inner">

        <div class="container overflow-hidden">
            <!--multisteps-form-->
            <div class="multisteps-form">
                <!--progress bar-->
                <div class="row">
                    <div class="col-12 col-lg-8 ml-auto mr-auto mb-4">
                        <div class="multisteps-form__progress">
                            <button class="multisteps-form__progress-btn js-active" id="main" type="button"
                                    title="User Info">
                                Source file
                            </button>
                            <button class="multisteps-form__progress-btn" type="button" title="Address">Target file
                            </button>
                            <button class="multisteps-form__progress-btn" type="button" title="Comments">Compare
                            </button>
                        </div>
                    </div>
                </div>
                <!--form panels-->
                <div class="row">
                    <div class="col-12 col-lg-8 m-auto">
                        <form id="dataForm" method="post" action="${pageContext.request.contextPath}/reconciliation"
                              enctype="multipart/form-data" class="multisteps-form__form" style="height: 329px;">
                            <!--single form panel-->
                            <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active"
                                 data-animation="scaleIn">
                                <h3 class="multisteps-form__title">Source File</h3>
                                <div class="multisteps-form__content">
                                    <div class="form-row mt-4">
                                        <div class="col-12 col-sm-8">
                                            <label for="sourceName">Source Name:</label>
                                            <input id="sourceName" name="sourceName"
                                                   class="multisteps-form__input form-control"
                                                   type="text" required>
                                        </div>
                                    </div>
                                    <div class="form-row mt-4">
                                        <div class="col-12 col-sm-8">
                                            <label for="sourceType">File type:</label>
                                            <select class="multisteps-form__select form-control" id="sourceType"
                                                    name="sourceType"
                                                    style="width: 100%; height: calc(1.5em + .75rem + 2px);background-color: darkgray">
                                                <option value="CSV">CSV</option>
                                                <option value="JSON">JSON</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row mt-4">
                                        <div class="col-12 col-sm-8 input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" required class="custom-file" name="sourceFile"
                                                       id="sourceFile">
                                                <label class="custom-file-label" for="sourceFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-row d-flex mt-4">
                                        <button class="btn btn-primary ml-auto js-btn-next" id="sourceNext" type="button" title="Next">
                                            Next
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--single form panel-->
                            <div class="multisteps-form__panel shadow p-4 rounded bg-white"
                                 data-animation="scaleIn">
                                <h3 class="multisteps-form__title">Target File</h3>
                                <div class="multisteps-form__content">
                                    <div class="form-row mt-4">
                                        <div class="col-12 col-sm-8">
                                            <label for="targetName">Target Name:</label>
                                            <input id="targetName" required name="targetName"
                                                   class="multisteps-form__input form-control"
                                                   type="text">
                                        </div>
                                    </div>
                                    <div class="form-row mt-4">
                                        <div class="col-12 col-sm-8">
                                            <label for="targetType">File type:</label>
                                            <select class="multisteps-form__select form-control" id="targetType"
                                                    name="targetType"
                                                    style="width: 100%; height: calc(1.5em + .75rem + 2px);background-color: darkgray">
                                                <option value="CSV">CSV</option>
                                                <option value="JSON">JSON</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row mt-4">
                                        <div class="col-12 col-sm-8 input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupFileAddon">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" required class="custom-file" name="targetFile"
                                                       id="targetFile">
                                                <label class="custom-file-label" for="targetFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-row d-flex mt-4">
                                        <button class="btn btn-light js-btn-prev" type="button" title="Prev">Prev
                                        </button>
                                        <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">
                                            Next
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--single form panel-->
                            <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                <h3 class="multisteps-form__title">Compare</h3>
                                <div class="multisteps-form__content">

                                    <div class="form-row mt-4" style="margin-left: 20%;">
                                        <div class="col-12 col-sm-4">
                                            <div class="card text-withe" style="height: 120px; border-radius: 0.5em">
                                                <div style="width: 100%; height: 20%; background-color: rgb(73, 115, 209); border-top-right-radius: 0.5em; border-top-left-radius: 0.5em">
                                                    <p style="color: white; margin-left: 35%">Source</p>
                                                </div>
                                                <div class="card-body" style="padding:1rem; font-size: 12px">
                                                    <p id="sourceNameLabel">Name: *****</p>
                                                    <p id="sourceTypeLabel">Type: CSV</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4" style="margin-left: 5%">
                                            <div class="card text-withe" style="height: 120px;border-radius: 0.5em">
                                                <div style="width: 100%; height: 20%; background-color: rgb(217, 235, 249); border-top-right-radius: 0.5em; border-top-left-radius: 0.5em">
                                                    <p style="color: rgb(129, 202, 235); margin-left: 35%">Target</p>
                                                </div>
                                                <div class="card-body" style="padding:1rem; font-size: 12px">
                                                    <p id="targetNameLabel">Name: *****</p>
                                                    <p id="targetTypeLabel">Type: CSV</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-row d-flex mt-4">
                                        <button class="btn btn-light js-btn-prev" type="button" title="Prev">
                                            Prev
                                        </button>
                                        <button id="cancel" class="btn btn-warning ml-auto" type="button"
                                                title="Cancel">Cancel
                                        </button>
                                        <button type="submit" class="btn btn-success ml-auto" id="submit"
                                                title="Compare">Compare
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>
<script src="/script/script.js"></script>


<script>

    $("#cancel").click(function () {
        $(this).closest('form').find("input[type=text], input[type=file]").val("");
        $('label[class*="custom-file-label"]').text('');
        $("#main").click();
    });

    $("#sourceName").change(function () {
        $("#sourceNameLabel").text("Name: " + $(this).val());
    });

    $("#sourceType").change(function () {
        $("#sourceTypeLabel").text("Type: " + $(this).val());
    });

    $("#targetName").change(function () {
        $("#targetNameLabel").text("Name: " + $(this).val());
    });

    $("#targetType").change(function () {
        $("#targetTypeLabel").text("Type: " + $(this).val());
    });

</script>
<session:CheckIfEndSession/>
</body>
</html>
