package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.data.FilesExtension;
import com.progressoft.jip9.compare.data.FilesType;
import com.progressoft.jip9.compare.interfaces.Comparator;
import com.progressoft.jip9.compare.interfaces.Printer;
import com.progressoft.jip9.compare.interfaces.Reader;
import com.progressoft.jip9.compare.request.PrinterRequest;
import com.progressoft.jip9.compare.request.ReaderRequest;
import com.progressoft.jip9.compare.result.CompareResult;
import com.progressoft.jip9.compare.result.PrinterResult;
import com.progressoft.jip9.compare.result.ReaderResult;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ConsoleTransactionCompare {

    private Reader reader;
    private Comparator comparator;
    private Printer printer;

    public ConsoleTransactionCompare(Reader reader, Comparator comparator, Printer printer) {
        this.reader = reader;
        this.comparator = comparator;
        this.printer = printer;
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        ReaderResult readSource = readFile(scanner, "Enter source file location:", "Enter source file format:", FilesType.SOURCE);
        ReaderResult readTarget = readFile(scanner, "Enter target file location:", "Enter target file format:", FilesType.TARGET);
        CompareResult compare = compare(readSource, readTarget);
        PrinterRequest printerRequest = getPrinterRequest(scanner);

        print(compare, printerRequest);
    }

    private PrinterRequest getPrinterRequest(Scanner scanner) {
        System.out.println("Please enter the path of directory to store results");
        String directory = scanner.nextLine();
        System.out.println("Please enter the name of match file");
        String match = scanner.nextLine();
        System.out.println("Please enter the name of mismatch file");
        String mismatch = scanner.nextLine();
        System.out.println("Please enter the name of missing file");
        String missing = scanner.nextLine();
        return new PrinterRequest(directory, match, mismatch, missing, FilesExtension.CSV);
    }

    private CompareResult compare(ReaderResult readSource, ReaderResult readTarget) {
        return comparator.compare(readSource, readTarget);
    }

    private void print(CompareResult compare, PrinterRequest printerRequest) {
        PrinterResult print = printer.print(compare, printerRequest);
        Path matching = print.getMatching();
        Path parent = matching.getParent();
        System.out.println("Reconciliation finished.");
        System.out.println("Result files are available in directory " + parent);
    }

    private ReaderResult readFile(Scanner scanner, String fileLocation, String fileFormat, FilesType type) {
        System.out.println(fileLocation);
        String sourcePath = scanner.nextLine();
        System.out.println(fileFormat);
        String sourceFormat = scanner.nextLine();
        ReaderRequest requestSource = new ReaderRequest(Paths.get(sourcePath), FilesExtension.parse(sourceFormat.toUpperCase()), type);
        return reader.read(requestSource);
    }
}
