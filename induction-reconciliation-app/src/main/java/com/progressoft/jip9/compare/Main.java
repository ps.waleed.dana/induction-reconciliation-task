package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.interfaces.Comparator;
import com.progressoft.jip9.compare.interfaces.Printer;
import com.progressoft.jip9.compare.interfaces.Reader;

public class Main {

    public static void main(String[] args) {
        Reader reader = new DefaultReader();
        Comparator comparator = new DefaultComparator();
        Printer printer = new DefaultPrinter();
        ConsoleTransactionCompare consoleTransactionCompare =
                new ConsoleTransactionCompare(reader, comparator, printer);
        consoleTransactionCompare.run();
    }
}