package com.progressoft.jip9.compare;

import com.progressoft.jip9.compare.implement.DBUsersProcessRepository;
import com.progressoft.jip9.compare.initialize.DatabaseInitializer;
import com.progressoft.jip9.compare.interfaces.UsersProcess;
import com.progressoft.jip9.compare.modul.User;

import javax.sql.DataSource;
import java.util.Scanner;

public class AddUser {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        DataSource dataSource = DatabaseInitializer.getMysqlDataSource();
        DatabaseInitializer databaseInitializer = new DatabaseInitializer(dataSource);
        UsersProcess users = new DBUsersProcessRepository(databaseInitializer, dataSource);
        System.out.println("username:");
        String username = scanner.nextLine();
        System.out.println("password:");
        String password = scanner.nextLine();
        User user = new User(username, password);
        users.addUser(user);
    }
}
